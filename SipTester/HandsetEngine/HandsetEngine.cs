﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Regus.SipTester
{
    public static class HandsetEngine
    {
        public static string SendConfigurationRequest(string ipAddress, string makeModel, string macAddress)
        {
            string make = makeModel.Substring(0, makeModel.IndexOf(' '));
            string model = makeModel.Substring(makeModel.IndexOf(' ')+1);

            return SendConfigurationRequest(ipAddress, make, model, macAddress);
        }
        
        public static string SendConfigurationRequest(string ipAddress, string make, string model, string macAddress)
        {
            make = ToTitleCase(make);
            model = model.ToUpper();

            string url = GenerateConfigurationURL("http://" + ipAddress + "/provisioning/", make, model, macAddress);

            using (WebClient client = new WebClient())
            {
                switch (make)
                {
                    case "Cisco":
                        client.Headers.Add("Host", ipAddress + ":80");
                        switch (model)
                        {
                            case "SPA508G":
                                client.Headers.Add("User-Agent", "Cisco/SPA508G-7.5.2b (" + macAddress + ")(CCC123456CC)");
                                client.Headers.Add("x-CiscoIPPhoneModelName", "SPA508G");
                                client.Headers.Add("x-CiscoIPPhoneDisplay", "128x64");
                                client.Headers.Add("x-CiscoIPPhoneSDKVersion", "NA");
                                break;
                            case "SPA525G":
                                client.Headers.Add("User-Agent", "Cisco/SPA525G-7.5.4 (AAA123456AA)");
                                break;
                            case "SPA525G2":
                            case "SPA525G2 + sidecar":
                                client.Headers.Add("User-Agent", "Cisco/SPA525G2-7.5.4 (BBB123456BB)");
                                break;
                        }
                        break;
                    case "Polycom":
                        client.Headers.Add("Host", ipAddress);
                        client.Headers.Add("Accept", "*/*");
                        switch (model)
                        {
                            case "IP330":
                                client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_330-UA/4.1.3.0052 Type/Application");
                                break;
                            case "IP331":
                                client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_331-UA/4.0.3.7562 Type/Application");
                                break;
                            case "IP450":
                                client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_450-UA/4.1.3.0052 Type/Application");
                                break;
                            case "VVX500":
                                client.Headers.Add("User-Agent", "PolycomSoundPointIP-VVX_500");
                                break;
                            case "SoundStation IP5000":
                                client.Headers.Add("User-Agent", "FileTransport PolycomSoundStationIP-SSIP_5000-UA/4.0.1.13681 Type/Application");
                                break;
                            case "SoundStation Duo":
                                client.Headers.Add("User-Agent", "FileTransport PolycomSoundStation-SS_Duo-UA/4.0.0.27587 Type/Application");
                                break;
                        }
                        break;
                    case "Snom":
                        client.Headers.Add("Host", ipAddress);
                        client.Headers.Add("Accept", "*/*");
                        client.Headers.Add("Accept-Language", "en");
                        client.Headers.Add("Keep-Alive", "5");
                        switch (model)
                        {
                            case "370":
                                client.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; snom370-SIP 8.4.35 1.1.3-u)");
                                break;
                        }
                        break;
                }
                try
                {
                    return client.DownloadString(url);
                }
                catch (WebException ex)
                {
                    return ex.Message;
                }
            }
        }

        private static string GenerateConfigurationURL(string provisionUrl, string make, string model, string macAddress)
        {
            string mac = macAddress.ToUpper();
            make = ToTitleCase(make);
            model = model.ToUpper();

            switch (make)
            {
                case "Cisco":
                    return provisionUrl + make + "/" + model + "/" + macAddress + ".cfg";
                case "Polycom":
                    return provisionUrl + macAddress + ".cfg";
                case "Snom":
                    return provisionUrl + "-" + macAddress;
            }
            return null;
        }

        private static string ToTitleCase(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            return text.Substring(0, 1).ToUpper() + text.Substring(1).ToLower();
        }
    }
}
