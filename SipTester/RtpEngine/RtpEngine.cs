﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LumiSoft.Net.RTP;
using LumiSoft.Net.Media.Codec.Audio;
using LumiSoft.Net.Media;
using System.Net.Sockets;
using System.Management;

namespace Regus.SipTester
{
    public class RtpEngine
    {
        private double bytesSent;
        public double BytesSent
        {
            get
            {
                if (sendStream != null && !sendStream.IsDisposed)
                    return sendStream.RtpBytesSent;
                else
                    return bytesSent;
            }
        }

        private double bytesReceived;
        public double BytesReceived
        {
            get
            {
                if (receiveStream != null && !receiveStream.IsDisposed)
                    return receiveStream.BytesReceived;
                else
                    return bytesReceived;
            }
        }

        private int ioExceptionCount;
        private Random random = new Random();

        private Dictionary<int, AudioCodec> audioCodecs;
        private AudioOut_RTP audioRTP;
        private AudioOut audio;
        private RTP_ReceiveStream receiveStream;
        private RTP_SendStream sendStream;
        private RTP_Session rtpSession;
        private RTP_MultimediaSession rtpSessions;

        private IPAddress localIP;
        private int localPort;
        private IPAddress remoteIP;
        private int remotePort;
        private string audioOutputDeviceName;
        private AudioOutDevice audioOutputDevice = null;
        private Thread audioThread;

        private string filesLocation;
        private string FilesLocation
        {
            get
            {
                return filesLocation;
            }
            set
            {
                filesLocation = value;
                if (filesLocation[filesLocation.Length - 1] != '\\')
                    filesLocation += '\\';
            }
        }

        public RtpEngine(IPAddress localIP, int localPort, IPAddress remoteIP, int remotePort, string filesLocation, string audioOutputDeviceName = null)
        {
            this.localIP = localIP;
            this.localPort = localPort;
            this.remoteIP = remoteIP;
            this.remotePort = remotePort;
            this.FilesLocation = filesLocation;
            this.audioOutputDeviceName = audioOutputDeviceName;

            if (audioOutputDeviceName != null)
            {
                foreach (AudioOutDevice audioDevice in AudioOut.Devices)
                {
                    if (String.Compare(audioDevice.Name, audioOutputDeviceName) == 0)
                    {
                        audioOutputDevice = audioDevice;
                        audioCodecs = new Dictionary<int, AudioCodec>();
                        audioCodecs.Add(0, new PCMU());
                        audio = new AudioOut(audioOutputDevice, 8000, 16, 1);
                        break;
                    }
                }
            }
        }

        public void StartAudio()
        {
            StopAudio();
            audioThread = new Thread(BeginTransmission);
            audioThread.Start();
        }

        public void StopAudio()
        {
            if (audioThread != null)
                audioThread.Abort();
        }

        private void BeginTransmission()
        {
            rtpSessions = new RTP_MultimediaSession(RTP_Utils.GenerateCNAME());
            rtpSessions.CreateSession(new RTP_Address(localIP, localPort, GetNextPort()), new RTP_Clock(0, 8000));
            rtpSessions.Sessions[0].AddTarget(new RTP_Address(remoteIP, remotePort, remotePort + 1));
            rtpSessions.Sessions[0].NewSendStream += new EventHandler<RTP_SendStreamEventArgs>(NewSendStream);

            if (audioOutputDevice != null)
                rtpSessions.Sessions[0].NewReceiveStream += new EventHandler<RTP_ReceiveStreamEventArgs>(NewReceiveStream);

            rtpSessions.Sessions[0].Payload = 0;
            rtpSessions.Sessions[0].Start();
            rtpSession = rtpSessions.Sessions[0];
            sendStream = rtpSession.CreateSendStream();
            AudioCodec codec = new PCMU();
            byte[] buffer = new byte[400];

            try
            {
                while (true)
                {
                    try
                    {
                        using (FileStream fileStream = File.OpenRead(GetAudioFile()))
                        {
                            while (fileStream.Read(buffer, 0, buffer.Length) > 0)
                            {
                                //Send audio frame
                                byte[] encodedData = codec.Encode(buffer, 0, buffer.Length);
                                RTP_Packet packet = new RTP_Packet();
                                packet.Timestamp = rtpSession.RtpClock.RtpTimestamp;
                                packet.Data = encodedData;
                                sendStream.Send(packet);
                                Thread.Sleep(25);
                            }
                            ioExceptionCount = 0;
                        }
                    }
                    catch (IOException)
                    {
                        ioExceptionCount++;
                        if (ioExceptionCount >= 10)
                            break;
                    }
                }
            }
            catch { }
            finally
            {
                if (sendStream != null)
                {
                    bytesSent = sendStream.RtpBytesSent;
                    sendStream.Close();
                }

                if (receiveStream != null)
                    bytesReceived = receiveStream.BytesReceived;

                if (rtpSessions != null)
                    rtpSessions.Close("Call Ended");
            }
        }

        private void NewSendStream(object sender, RTP_SendStreamEventArgs e)
        {
        }

        private void NewReceiveStream(object sender, RTP_ReceiveStreamEventArgs e)
        {
            if (audioOutputDevice != null)
            {
                try
                {
                    receiveStream = e.Stream;
                    audioRTP = new AudioOut_RTP(audioOutputDevice, receiveStream, audioCodecs);
                    audioRTP.Start();
                }
                catch (Exception)
                { }
            }
        }

        private int GetNextPort()
        {
            TcpListener listener = new TcpListener(localIP, 0);
            listener.Start();
            int port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }

        private string GetAudioFile()
        {
            string[] filePaths = Directory.GetFiles(FilesLocation, "*.raw");
            if (filePaths.Length > 0)
            {
                return filePaths[random.Next(0, filePaths.Length - 1)];
            }
            return null;
        }
    }
}
