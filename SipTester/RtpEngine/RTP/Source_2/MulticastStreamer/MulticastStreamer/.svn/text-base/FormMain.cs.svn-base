﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace MulticastStreamer
{
	/// <summary>
	/// FormMain
	/// </summary>
	public partial class FormMain : Form
	{
		/// <summary>
		/// Konstruktor
		/// </summary>
		public FormMain()
		{
			InitializeComponent();
			Init();
		}

		//Attribute
		private NF.MulticastSender m_MulticastSender;
		private WinSound.Recorder m_Recorder = new WinSound.Recorder();
		private Configuration Config = new Configuration();
		private String ConfigFileName = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "config.xml");
		private WinSound.RTPPacket RTPHeader = new WinSound.RTPPacket();

		/// <summary>
		/// Init
		/// </summary>
		private void Init()
		{
			try
			{
				InitRTPHeader();
				InitComboboxes();
				LoadConfig();
				InitRecorder();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Init()", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		/// <summary>
		/// InitRTPHeader
		/// </summary>
		private void InitRTPHeader()
		{
			RTPHeader.Version = 2;
			RTPHeader.Padding = false;
			RTPHeader.Extension = false;
			RTPHeader.CSRCCount = 0;
			RTPHeader.Marker = false;
			RTPHeader.PayloadType = 0;
			RTPHeader.SequenceNumber = Convert.ToUInt16(new Random(0).Next(System.UInt16.MaxValue));
			RTPHeader.Timestamp = Convert.ToUInt32(new Random(0).Next());
			RTPHeader.SourceId = 0;
		}
		/// <summary>
		/// InitRecorder
		/// </summary>
		private void InitRecorder()
		{
			m_Recorder.DataRecorded += new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard);
			m_Recorder.RecordingStopped += new WinSound.Recorder.DelegateStopped(OnRecordingStopped);
		}
		/// <summary>
		/// InitComboboxes
		/// </summary>
		private void InitComboboxes()
		{
			ComboboxSoundDeviceName.Items.Clear();
			List<String> names = WinSound.WinSound.GetRecordingNames();

			foreach (String name in names.Where(x => x != null))
			{
				ComboboxSoundDeviceName.Items.Add(name);
			}

			if (ComboboxSoundDeviceName.Items.Count > 0)
			{
				ComboboxSoundDeviceName.SelectedIndex = 0;
			}
			ComboboxSamplesPerSecond.SelectedIndex = 1;
			ComboboxBitsPerSample.SelectedIndex = 0;
			ComboboxChannels.SelectedIndex = 0;
			ComboboxBufferCount.SelectedIndex = 4;
		}
		/// <summary>
		/// Config
		/// </summary>
		public class Configuration
		{
			/// <summary>
			/// Config
			/// </summary>
			public Configuration()
			{

			}

			//Attribute
			public String MulticasAddress = "";
			public String SoundDeviceName = "";
			public int MulticastPort = 0;
			public int SamplesPerSecond = 8000;
			public short BitsPerSample = 16;
			public short Channels = 2;
			public Int32 PacketSize = 1024;
			public Int32 BufferCount = 8;
		}
		//----------------------------------------------------------------
		//Daten schreiben
		//----------------------------------------------------------------
		private void SaveConfig()
		{
			try
			{
				FormToConfig();
				XmlSerializer ser = new XmlSerializer(typeof(Configuration));
				FileStream stream = new FileStream(ConfigFileName, FileMode.Create);
				ser.Serialize(stream, this.Config);
				stream.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		//----------------------------------------------------------------
		//Daten lesen
		//---------------------------------------------------------------- 
		private void LoadConfig()
		{
			try
			{
				//Wenn die Datei existiert
				if (File.Exists(ConfigFileName))
				{
					XmlSerializer ser = new XmlSerializer(typeof(Configuration));
					StreamReader sr = new StreamReader(ConfigFileName);
					Config = (Configuration)ser.Deserialize(sr);
					sr.Close();
				}

				//Daten anzeigen
				ConfigToForm();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		/// <summary>
		/// FormToConfig
		/// </summary>
		/// <returns></returns>
		private bool FormToConfig()
		{
			try
			{
				Config.SoundDeviceName = ComboboxSoundDeviceName.SelectedItem.ToString();
				Config.MulticasAddress = TextBoxMulticastAddress.Text;
				Config.MulticastPort = Convert.ToInt32(TextBoxMulticastPort.Text);
				Config.SamplesPerSecond = Convert.ToInt32(ComboboxSamplesPerSecond.SelectedItem.ToString());
				Config.BitsPerSample = Convert.ToInt16(ComboboxBitsPerSample.SelectedItem.ToString());
				Config.Channels = Convert.ToInt16(ComboboxChannels.SelectedItem.ToString());
				Config.PacketSize = (Int32)NumericUpDownPacketSize.Value;
				Config.BufferCount = Convert.ToInt32(ComboboxBufferCount.SelectedItem.ToString());
				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Fehler bei der Eingabe", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}
		/// <summary>
		/// ConfigToForm
		/// </summary>
		/// <returns></returns>
		private bool ConfigToForm()
		{
			try
			{
				ComboboxSoundDeviceName.SelectedIndex = ComboboxSoundDeviceName.FindString(Config.SoundDeviceName);
				TextBoxMulticastAddress.Text = Config.MulticasAddress;
				TextBoxMulticastPort.Text = Config.MulticastPort.ToString();
				ComboboxSamplesPerSecond.SelectedIndex = ComboboxSamplesPerSecond.FindString(Config.SamplesPerSecond.ToString());
				ComboboxBitsPerSample.SelectedIndex = ComboboxBitsPerSample.FindString(Config.BitsPerSample.ToString());
				ComboboxChannels.SelectedIndex = ComboboxChannels.FindString(Config.Channels.ToString());
				NumericUpDownPacketSize.Value = Config.PacketSize;
				ComboboxBufferCount.SelectedIndex = ComboboxBufferCount.FindString(Config.BufferCount.ToString());
				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return false;
			}
		}
		/// <summary>
		/// StartRecording
		/// </summary>
		private void StartRecording()
		{
			try
			{
				//Buffer Grösse berechnen
				int bufferSize = Config.PacketSize - WinSound.RTPPacket.HeaderLength;
				bufferSize = (bufferSize * Config.Channels * (Config.BitsPerSample / 8));

				if (bufferSize > 0)
				{
					if (m_Recorder.Start(Config.SoundDeviceName, Config.SamplesPerSecond, Config.BitsPerSample, Config.Channels, Config.BufferCount, bufferSize))
					{
						ShowStarted();
					}
					else
					{
						ShowError();
					}
				}
				else
				{
					System.Diagnostics.Debug.WriteLine(String.Format("BufferSize must be greater than 0. BufferSize: {0}", bufferSize));
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		/// <summary>
		/// OnDataReceivedFromSoundcard
		/// </summary>
		/// <param name="data"></param>
		private void OnDataReceivedFromSoundcard(Byte[] data)
		{
			try
			{
				lock (this)
				{
					if (m_MulticastSender != null)
					{
						//Daten Nach MuLaw umwandeln
						Byte[] mulaws = WinSound.Utils.LinearToMulaw(data, Config.BitsPerSample, Config.Channels);

						//RTP Header aktualisieren
						try
						{
							RTPHeader.SequenceNumber = Convert.ToUInt16(RTPHeader.SequenceNumber + 1);
						}
						catch (Exception)
						{
							RTPHeader.SequenceNumber = 0;
						}
						try
						{
							RTPHeader.Timestamp = Convert.ToUInt32(RTPHeader.Timestamp + mulaws.Length);
						}
						catch (Exception)
						{
							RTPHeader.Timestamp = 0;
						}

						//RTPHeader in Bytes erstellen
						Byte[] rtpBytes = RTPHeader.ToBytes();

						//RTP Header und Daten zusammenlegen
						Byte[] bytes = new Byte[mulaws.Length + WinSound.RTPPacket.HeaderLength];
						Array.Copy(rtpBytes, 0, bytes, 0, WinSound.RTPPacket.HeaderLength);
						Array.Copy(mulaws, 0, bytes, WinSound.RTPPacket.HeaderLength, mulaws.Length);

						//Abschicken
						m_MulticastSender.SendBytes(bytes);
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}
		}
		/// <summary>
		/// OnRecordingStopped
		/// </summary>
		private void OnRecordingStopped()
		{
			try
			{
				this.Invoke(new MethodInvoker(delegate()
				{
					ShowStopped();
				}));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}
		}
		/// <summary>
		/// ShowStarted
		/// </summary>
		private void ShowStarted()
		{
			ButtonStart.BackColor = Color.DarkGreen;
			foreach (Control ctrl in this.Controls)
			{
				if (ctrl.GetType() == typeof(GroupBox))
				{
					foreach (Control ctrlNext in ctrl.Controls)
					{
						if (ctrlNext.GetType() == typeof(ComboBox))
						{
							ctrlNext.Enabled = false;
						}
						else if (ctrlNext.GetType() == typeof(NumericUpDown))
						{
							ctrlNext.Enabled = false;
						}
						else if (ctrlNext.GetType() == typeof(TextBox))
						{
							ctrlNext.Enabled = false;
						}
					}
				}
			}
		}
		/// <summary>
		/// ShowStopped
		/// </summary>
		private void ShowStopped()
		{
			ButtonStart.BackColor = SystemColors.Control;
			foreach (Control ctrl in this.Controls)
			{
				if (ctrl.GetType() == typeof(GroupBox))
				{
					foreach (Control ctrlNext in ctrl.Controls)
					{
						if (ctrlNext.GetType() == typeof(ComboBox))
						{
							ctrlNext.Enabled = true;
						}
						else if (ctrlNext.GetType() == typeof(NumericUpDown))
						{
							ctrlNext.Enabled = true;
						}
						else if (ctrlNext.GetType() == typeof(TextBox))
						{
							ctrlNext.Enabled = true;
						}
					}
				}
			}
		}
		/// <summary>
		/// ShowError
		/// </summary>
		private void ShowError()
		{
			ButtonStart.BackColor = Color.Red;
			foreach (Control ctrl in this.Controls)
			{
				if (ctrl.GetType() == typeof(GroupBox))
				{
					foreach (Control ctrlNext in ctrl.Controls)
					{
						if (ctrlNext.GetType() == typeof(ComboBox))
						{
							ctrlNext.Enabled = true;
						}
						else if (ctrlNext.GetType() == typeof(NumericUpDown))
						{
							ctrlNext.Enabled = true;
						}
						else if (ctrlNext.GetType() == typeof(TextBox))
						{
							ctrlNext.Enabled = true;
						}
					}
				}
			}
		}
		/// <summary>
		/// FormMain_FormClosing
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			try
			{
				if (m_MulticastSender != null)
				{
					m_MulticastSender.Close();
				}

				m_Recorder.DataRecorded -= new WinSound.Recorder.DelegateDataRecorded(OnDataReceivedFromSoundcard);
				m_Recorder.RecordingStopped -= new WinSound.Recorder.DelegateStopped(OnRecordingStopped);
				m_Recorder.Stop();
				SaveConfig();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		/// <summary>
		///  ButtonStart_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ButtonStart_Click(object sender, EventArgs e)
		{
			try
			{
				//Daten ermitteln
				FormToConfig();

				if (m_Recorder.Started == false)
				{
					//Starten
					m_MulticastSender = new NF.MulticastSender(Config.MulticasAddress, Config.MulticastPort, 10);
					StartRecording();
					ShowStarted();
				}
				else
				{
					//Schliessen
					m_MulticastSender.Close();
					m_MulticastSender = null;
					m_Recorder.Stop();

					//Warten bis Aufnahme beendet
					while (m_Recorder.Started)
					{
						Application.DoEvents();
						System.Threading.Thread.Sleep(100);
					}
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}

