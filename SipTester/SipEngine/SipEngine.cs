﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.ComponentModel;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using Independentsoft.Sip;
using Independentsoft.Sip.Sdp;
using Independentsoft.Sip.Methods;
using Independentsoft.Sip.Responses;
using System.Net;
using System.Globalization;

namespace Regus.SipTester
{
    public class SipEngine : INotifyPropertyChanged
    {
        #region Public Members
        public event PropertyChangedEventHandler PropertyChanged;

        public bool PlainLogging { get; set; }
        public bool SipLogging { get; set; }
        public int AnswerDelayMS { get; set; }
        public int SessionVersion { get; set; }
        public string SessionName { get; set; }
        public string FilePath { get; set; }
        public int DialToneDelayMS { get; set; }
        public SipClient Client { get; private set; }
        public string ForwardTo { get; set; }
        public int ExpiryTime { get; private set; }
        public string AudioOutputDevice { get; set; }
        public string AudioFilePath { get; set; }
        public bool DisconnectAfterCall { get; set; }
        public double VoicemailMessagesUnread { get; private set; }
        public double VoicemailMessagesRead { get; private set; }
        public double VoicemailUrgentMessagesUnread { get; private set; }
        public double VoicemailUrgentMessagesRead { get; private set; }
        private string contact;
        public string Contact
        {
            get
            {
                return contact;
            }
            private set
            {
                if (string.Compare(value, contact) != 0)
                {
                    this.contact = value;
                    NotifyPropertyChanged("Contact");
                }
            }
        }

        public ObservableCollection<TraceMessage> MessageLog { get; private set; }
        private bool isConnected;
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
            private set
            {
                if (value != isConnected)
                {
                    this.isConnected = value;
                    NotifyPropertyChanged("IsConnected");
                    CanConnect = !isConnected;
                    CanCall = isConnected;
                }
            }
        }
        private bool canConnect;
        public bool CanConnect
        {
            get
            {
                return canConnect;
            }
            private set
            {
                this.canConnect = value;
                NotifyPropertyChanged("CanConnect");
            }
        }
        private bool onCall;
        public bool OnCall
        {
            get
            {
                return onCall;
            }
            private set
            {
                if (value != onCall)
                {
                    this.onCall = value;
                    NotifyPropertyChanged("OnCall");
                    CanCall = !onCall;

                    if (DisconnectAfterCall && !OnCall)
                    {
                        Thread disconnectThread = new Thread(Disconnect);
                        disconnectThread.Start();
                    }
                }
            }
        }
        private bool canCall;
        public bool CanCall
        {
            get
            {
                return canCall;
            }
            private set
            {
                this.canCall = value;
                NotifyPropertyChanged("CanCall");
            }
        }
        private bool unansweredCall;
        public bool UnansweredCall
        {
            get
            {
                return unansweredCall;
            }
            private set
            {
                this.unansweredCall = value;
                NotifyPropertyChanged("UnansweredCall");
            }
        }
        public Call LastCall
        {
            get
            {
                if (currentCalls.Count > 0)
                    return currentCalls.Last();
                else if (callHistory.Count > 0)
                    return callHistory.Last();
                else
                    return null;
            }
        }

        private bool sendAudio;
        public bool SendAudio
        {
            get
            {
                return sendAudio;
            }
            set
            {
                if (value)
                    StartAudio();
                else
                    StopAudio();

                this.sendAudio = value;
                NotifyPropertyChanged("SendAudio");
            }
        }
        #endregion

        #region Private Members
        private Collection<Request> incomingInvites;
        private Collection<Call> currentCalls;
        private Collection<Call> callHistory;
        private BindingList<String> logFileMessages;

        private bool logCreated;
        private Thread registerThread;
        private delegate void saveMessage(String message);
        private static ManualResetEvent signal = new ManualResetEvent(false);
        private Thread callThread;
        #endregion

        #region Constructors
        public SipEngine() : this(true, true) { }

        public SipEngine(bool pLog, bool sLog)
        {
            MessageLog = new ObservableCollection<TraceMessage>();
            incomingInvites = new Collection<Request>();
            currentCalls = new Collection<Call>();
            callHistory = new Collection<Call>();
            DisconnectAfterCall = false;
            logFileMessages = new BindingList<string>();
            logFileMessages.ListChanged += logFileMessages_ListChanged;
            PlainLogging = pLog;
            SipLogging = sLog;
            SessionVersion = 2;
            SessionName = "SipTester Call";
            CanConnect = true;
            SendAudio = true;
            VoicemailMessagesUnread = 0;
            VoicemailMessagesRead = 0;
            VoicemailUrgentMessagesUnread = 0;
            VoicemailUrgentMessagesRead = 0;
        }
        #endregion

        #region Public Methods
        public bool Connect(string serverIP, string username, string password, string networkAdaptorID, int port, bool waitToConnect = false)
        {
            CreateLogFile();

            Client = new SipClient(serverIP, username, password);
            Client.LocalIPEndPoint = NetworkEngine.GetLocalEndPoint(networkAdaptorID, port);

            if (Client.LocalIPEndPoint == null)
                TextLog("MSG: No network adaptor selected.");

            //Create Logger
            Logger logger = new Logger();
            logger.WriteLog += new WriteLogEventHandler(OnWriteLog);
            Client.Logger = logger;

            //Register request and response handlers
            Client.ReceiveRequest += new ReceiveRequestEventHandler(OnReceiveRequest);
            Client.ReceiveResponse += new ReceiveResponseEventHandler(OnReceiveResponse);

            //Connect client
            if (waitToConnect)
            {
                Connect();
                return IsConnected;
            }
            else
            {
                Thread connectThread = new Thread(Connect);
                connectThread.Start();
                return false;
            }
        }

        public void Disconnect()
        {
            if (!IsConnected)
                return;

            try
            {
                EndOutgoingCalls();
                incomingInvites.Clear();
                UpdateIncomingInvites();

                currentCalls.Clear();
                UpdateOnCall();

                TextLog("MSG: Deregistering...");
                registerThread.Abort();
                Client.Unregister("sip:" + Client.Domain, "sip:" + Client.Username + "@" + Client.Domain, "sip:" + Client.Username + "@" + Client.Domain);
            }
            catch (Exception e)
            {
                Exception ex = e;
            }
            finally
            {
                TextLog("MSG: Disconnecting...");
                Client.Disconnect();

                Client = null;
                IsConnected = false;
            }
        }

        public Call Call(string toAddress, bool waitForAnswer = true)
        {
            if (currentCalls.Count > 0)
                HoldCall(currentCalls.Last());

            TextLog("MSG: Calling " + toAddress + "...");

            Call call = new Call(toAddress);
            currentCalls.Add(call);
            UpdateOnCall();

            if (waitForAnswer)
                MakeCall(call);
            else
            {
                callThread = new Thread(MakeCall);
                callThread.Start(call);
            }
            return call;
        }

        private void MakeCall(object callObject)
        {
            Call call = (Call)callObject;
            MakeCall(call);
        }

        private void MakeCall(Call call)
        {
            string from = "sip:" + Client.Username + "@" + Client.Domain;
            string to = "sip:" + call.ToAddress + "@" + Client.Domain;

            //Initiate call and wait for the callee to pick up
            Invite invite = new Invite();
            invite.Uri = to;
            invite.From = new ContactInfo(from);
            invite.To = new ContactInfo(to);
            invite.Contact = new Contact(Contact);
            invite.ContentType = "application/sdp";
            SessionDescription session = GenerateSessionDescription();
            invite.Body = session.ToString();
            
            call.RequestResponse = Client.SendRequest(invite);

            if (Client.GetDialog(call.RequestResponse) != null)
            {
                Client.Ack(call.RequestResponse);
                call.IsConnected = true;
                TextLog("MSG: ON CALL (" + call.CallID + ")");
                SendReceiveAudio(call);
            }
        }

        public void TransferCall(string transferTo)
        {
            if (currentCalls.Count > 1)
            {
                //HoldCall(LastCall);
                currentCalls[0].RequestResponse = Client.Refer(
                    Client.GetDialog(currentCalls[0].RequestResponse), "sip:" + transferTo + "@" + Client.Domain);
            }
            else
            {
                LastCall.RequestResponse = Client.Refer(Client.GetDialog(LastCall.RequestResponse), "sip:" + transferTo + "@" + Client.Domain);
            }
        }

        public void HoldCall(Call call)
        {          
            if (!call.OnHold)
            {
                SessionDescription session = call.SessionDescription;
                session.Media[0].Attributes.Remove("sendrecv");
                session.Media[0].Attributes.Add("sendonly");
                call.RequestResponse = Client.Reinvite(Client.GetDialog(call.RequestResponse), session);
                Client.Ack(call.RequestResponse);
                call.OnHold = true;
            }
        }

        public void ResumeHeldCall(Call call)
        {
            if (call.OnHold)
            {
                SessionDescription session = call.SessionDescription;
                session.Media[0].Attributes.Remove("sendonly");
                session.Media[0].Attributes.Remove("recvonly");
                session.Media[0].Attributes.Add("sendrecv");
                call.RequestResponse = Client.Reinvite(Client.GetDialog(call.RequestResponse), session);
                Client.Ack(call.RequestResponse);
                call.OnHold = false;
            }
        }

        public string GetRemoteNumber(Call call)
        {
            Dialog dialog = Client.GetDialog(call.RequestResponse);
            string number = dialog.RemoteUri;
            number = number.Replace("sip:", "");
            return number.Substring(0, number.IndexOf('@'));
        }

        public void EndOutgoingCalls()
        {
            foreach (Call call in currentCalls.ToList<Call>())
            {
                if (!call.Incoming)
                    EndCall(call);
            }
        }

        public void EndCalls()
        {
            foreach (Call call in currentCalls.ToList<Call>())
            {
                EndCall(call);
            }
        }

        public void EndCall()
        {
            if (currentCalls.Count > 0)
                EndCall(currentCalls.Last());
        }

        public void EndCall(Call call)
        {
            currentCalls.Remove(call);
            UpdateOnCall();

            //End voice transmission
            if (call.RtpEngine != null)
            {
                TextLog("MSG: CALL ENDED (" + call.CallID + ")" + Environment.NewLine
                    + "Bytes Sent: " + call.RtpEngine.BytesSent + Environment.NewLine
                    + "Bytes Received: " + call.RtpEngine.BytesReceived);
                call.RtpEngine.StopAudio();
            }
            else if (!call.IsConnected && callThread.IsAlive)
            {
                callThread.Abort();
                TextLog("MSG: CALL ENDED");
            }
            else if (!String.IsNullOrWhiteSpace(call.CallID))
            {
                TextLog("MSG: CALL ENDED (" + call.CallID + ")");
            }
            else
            {
                TextLog("MSG: CALL ENDED");
            }

            //End call
            if (call.RequestResponse != null)
            {
                Client.Bye(call.RequestResponse);
            }
            else
            {
                Client.Cancel();
            }

            call.IsConnected = false;
            callHistory.Add(call);
        }

        public Call AnswerNextCall()
        {
            if (incomingInvites.Count > 0)
            {
                Request request = incomingInvites.Last();
                return AnswerCall(request);
            }
            return null;
        }

        public void StartAudio()
        {
            foreach(Call call in currentCalls)
            {
                call.RtpEngine.StartAudio();
            }
        }

        public void StopAudio()
        {
            foreach (Call call in currentCalls)
            {
                call.RtpEngine.StopAudio();
            }
        }

        private Call AnswerCall(Request request)
        {
            incomingInvites.Remove(request);
            UpdateIncomingInvites();

            Independentsoft.Sip.Sdp.AttributeCollection attributes = request.SessionDescription.Media[0].Attributes;

            if (attributes.Contains("sendonly"))
            {
                attributes.Remove("sendonly");
                attributes.Add("recvonly");
            }

            OK okResponse = new OK();
            okResponse.SessionDescription = GenerateSessionDescription();
            okResponse.Contact = Client.Contact;
            Call call = new Call(request, Client.SendResponse(okResponse, request));
            SendReceiveAudio(call);
            currentCalls.Add(call);
            UpdateOnCall();
            return call;
        }

        public void SendTone(char digit)
        {
            if (currentCalls.Count > 0)
                SendTone(currentCalls.Last(), digit);
        }

        public void SendTone(Call call, char digit)
        {
            if (call.IsConnected)
            {
                TextLog("MSG: Sending Dial Tone: " + digit);
                Info info = new Info(Client.GetDialog(call.RequestResponse));
                info.ContentType = "application/dtmf-relay";
                info.Body = "Signal=" + digit + Environment.NewLine;
                Client.SendRequest(info);
                Thread.Sleep(DialToneDelayMS);
            }
        }

        public void SendTones(string digits)
        {
            if (currentCalls.Count > 0)
                SendTones(currentCalls.Last(), digits);
        }

        public void SendTones(Call call, string digits)
        {
            foreach (char digit in digits)
            {
                SendTone(call, digit);
            }
        }

        public void RequestVoicemailMessageSummary()
        {
            ReRegister(true);
        }

        public void TextLog(string message)
        {
            if (PlainLogging)
            {
                WriteFileLog(message);
                WriteScreenLog(message);
            }
        }

        public void ClearLog()
        {
            MessageLog.Clear();
        }

        public void UpdateVoicemailMessageCounts()
        {
            while (true)
            {
                signal.Reset();
                ReRegister(true);
                if (signal.WaitOne(100))
                    break;
            }
        }
        #endregion

        #region Event Handlers
        private void OnReceiveRequest(object sender, RequestEventArgs e)
        {
            //Accept request from server or another sip user agent
            switch (e.Request.Method)
            {
                case SipMethod.Invite:
                    TextLog("REQ: " + e.Request.Method + " - from: " + e.Request.From.Address + " " + e.Request.From.Name);
                    Client.SendResponseRinging(e.Request);

                    if (String.IsNullOrWhiteSpace(ForwardTo))
                    {
                        TextLog("MSG: RING RING");
                        incomingInvites.Add(e.Request);
                        UnansweredCall = true;

                        if (AnswerDelayMS > 0)
                        {
                            TextLog("MSG: Waiting " + AnswerDelayMS + "ms before answering...");
                            Thread.Sleep((int)AnswerDelayMS);
                            AnswerCall(e.Request);
                        }
                    }
                    else
                    {
                        Thread forwardThread = new Thread(Forward);
                        forwardThread.Start(e.Request);
                    }
                    break;

                case SipMethod.Ack:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method);

                    Call currentCall = FindCall(e.Request.CallID);
                    if (currentCall != null)
                    {
                        currentCall.IsConnected = true;
                        TextLog("MSG: ON CALL (" + currentCall.CallID + ")");
                    }
                    break;

                case SipMethod.Bye:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method + " - from: " + e.Request.From.Address + " " + e.Request.From.Name);
                    
                    currentCall = FindCall(e.Request.CallID);
                    
                    if (currentCall != null)
                    {
                        currentCalls.Remove(currentCall);
                        UpdateOnCall();
                        if (currentCall.RtpEngine != null)
                        {
                            TextLog("MSG: CALL ENDED (" + currentCall.CallID + ")" + Environment.NewLine
                                + "Bytes Sent: " + currentCall.RtpEngine.BytesSent + Environment.NewLine
                                + "Bytes Received: " + currentCall.RtpEngine.BytesReceived);
                            currentCall.RtpEngine.StopAudio();
                        }
                        else if (!String.IsNullOrWhiteSpace(currentCall.CallID))
                        {
                            TextLog("MSG: CALL ENDED (" + currentCall.CallID + ")");
                        }
                        else
                        {
                            TextLog("MSG: CALL ENDED");
                        }

                        currentCall.IsConnected = false;
                        callHistory.Add(currentCall);
                    }
                    break;

                case SipMethod.Info:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method + " - from: " + e.Request.From.Address + " " + e.Request.From.Name + ":" + Environment.NewLine + e.Request.Body);
                    break;

                case SipMethod.Notify:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method + " - " + e.Request.Body);
                    if (e.Request.Body.Contains("Voice-Message:"))
                    {
                        String messageString = e.Request.Body.Substring(e.Request.Body.IndexOf("Voice-Message:") + 15);
                        messageString = messageString.Substring(0, messageString.IndexOf(')'));
                        Regex regex = new Regex(@"(\d+)(/)(\d+)(.*)(\()(\d+)(/)(\d+)");
                        MatchCollection matches = regex.Matches(messageString);
                        VoicemailMessagesUnread = Convert.ToDouble(matches[0].Groups[1].Value);
                        VoicemailMessagesRead = Convert.ToDouble(matches[0].Groups[3].Value);
                        VoicemailUrgentMessagesUnread = Convert.ToDouble(matches[0].Groups[6].Value);
                        VoicemailUrgentMessagesRead = Convert.ToDouble(matches[0].Groups[8].Value);
                        signal.Set();
                    }
                    break;

                case SipMethod.Options:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method);
                    break;

                default:
                    Client.AcceptRequest(e.Request);
                    TextLog("REQ: " + e.Request.Method);
                    break;
            }
        }

        private void OnReceiveResponse(object sender, ResponseEventArgs e)
        {
            TextLog("RSP: " + e.Response.StatusCode + " - " + e.Response.Description);
        }

        private void OnWriteLog(object sender, WriteLogEventArgs e)
        {
            if (SipLogging && !String.IsNullOrWhiteSpace(e.Log))
                WriteFileLog(Environment.NewLine + e.Log);
        }

        private void NotifyPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                 handler(this, new PropertyChangedEventArgs(name));
            }
        }

        void logFileMessages_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                //Write message to log file
                using (StreamWriter streamWriter = new StreamWriter(FilePath, true))
                {
                    streamWriter.WriteLine("> {0} {1} {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), logFileMessages.First());
                    logFileMessages.Remove(logFileMessages.First());
                }
            }
        }
        #endregion

        #region Private Methods
        
        private void Connect()
        {
            try
            {
                Client.Connect();
            }
            catch (Exception e)
            {
                TextLog("MSG: Exception encountered:" + Environment.NewLine + e.Message);
                Client = null;
                return;
            }

            TextLog("MSG: Registering location " + Client.LocalIPEndPoint.ToString() + "...");
            if (Register())
            {
                registerThread = new Thread(ReRegister);
                registerThread.Start();
            }
        }

        private Call FindCall(string callID)
        {
            foreach (Call call in currentCalls)
            {
                if (call.CallID == callID)
                    return call;
            }
            return null;
        }

        private Request FindIncommingInvite(string callID)
        {
            foreach (Request request in incomingInvites)
            {
                if (request.CallID == callID)
                    return request;
            }
            return null;
        }

        private Call FindHistoricCall(string callID)
        {
            foreach (Call call in callHistory)
            {
                if (call.CallID == callID)
                    return call;
            }
            return null;
        }

        private SessionDescription GenerateSessionDescription()
        {
            Owner owner = new Owner();
            owner.Username = Client.Username;
            owner.Address = Client.LocalIPEndPoint.Address.ToString();

            Connection connection = new Connection();
            connection.Address = Client.LocalIPEndPoint.Address.ToString();

            Time time = new Time(0, 0);

            SessionDescription session = new SessionDescription();
            session.Version = 0;
            session.Name = SessionName;
            session.Owner = owner;
            session.Connection = connection;
            session.Time.Add(time);
            session.Media.Add(GenerateMediaDescription());

            return session;
        }

        private Media GenerateMediaDescription()
        {
            Media media = new Media();
            media.Type = "audio";
            media.TransportProtocol = "RTP/AVP";
            media.MediaFormats.Add("0");
            media.MediaFormats.Add("101");
            media.Attributes.Add("rtpmap", "0 pcmu/8000");
            media.Attributes.Add("rtpmap", "101 telephone-event/8000");
            media.Attributes.Add("fmtp", "101 0-11");
            media.Attributes.Add("sendrecv");
            media.Port = NetworkEngine.GetNextPort(Client.LocalIPEndPoint.Address);
            return media;
        }

        private bool Register()
        {
            //Register user to IP address
            IsConnected = false;
            string domain = "sip:" + Client.Domain;
            string from = "sip:" + Client.Username + "@" + Client.Domain;
            Contact = "sip:" + Client.Username + "@" + Client.LocalIPEndPoint.ToString();
            //Register location of client

            try
            {
                RequestResponse registerResponse = Client.Register(domain, from, Contact);
                if (registerResponse.Response.StatusCode == 200)
                {
                    ExpiryTime = Convert.ToInt32(registerResponse.Response.Contact.Expires);
                    IsConnected = true;
                }
            }
            catch (Exception e)
            {
                TextLog("MSG: Exception encountered:" + Environment.NewLine + e.Message);
            }
            return IsConnected;
        }

        private void ReRegister()
        {
            ReRegister(false);
        }

        private void ReRegister(bool once)
        {
            while (Client != null && IsConnected)
            {
                if (!once)
                    Thread.Sleep((int)(ExpiryTime * 1000));

                //Register user to IP address
                TextLog("MSG: Re-registering...");
                string domain = "sip:" + Client.Domain;
                string from = "sip:" + Client.Username + "@" + Client.Domain;
                try
                {
                    //Register location of client
                    RequestResponse reRegisterResponse = Client.Register(domain, from, Contact);
                    if (reRegisterResponse.Response.StatusCode == 200)
                        ExpiryTime = Convert.ToInt32(reRegisterResponse.Response.Contact.Expires);
                    else
                        IsConnected = false;
                }
                catch (Exception e)
                {
                    TextLog("MSG: Exception encountered:" + Environment.NewLine + e.Message);
                }

                if (once)
                    return;
            }
        }

        private void SendReceiveAudio(Call call)
        {
            Dialog dialog = Client.GetDialog(call.RequestResponse);
            string remoteUri = dialog.Contact.Address;
            remoteUri = remoteUri.Substring(remoteUri.IndexOf('@') + 1);
            if (remoteUri.IndexOf(':') > 0)
                remoteUri = remoteUri.Substring(0, remoteUri.IndexOf(':'));

            IPAddress remoteIP = IPAddress.Parse(remoteUri);

            int remotePort;
            int localPort;

            if (call.Incoming)
            {
                remotePort = call.RequestResponse.Request.SessionDescription.Media[0].Port;
                localPort = call.RequestResponse.Response.SessionDescription.Media[0].Port;
            }
            else
            {
                remotePort = call.RequestResponse.Response.SessionDescription.Media[0].Port;
                localPort = call.RequestResponse.Request.SessionDescription.Media[0].Port;
            }

            if (!String.IsNullOrWhiteSpace(AudioOutputDevice))
                call.RtpEngine = new RtpEngine(Client.LocalIPEndPoint.Address, localPort, remoteIP, remotePort, AudioFilePath, AudioOutputDevice);
            else
                call.RtpEngine = new RtpEngine(Client.LocalIPEndPoint.Address, localPort, remoteIP, remotePort, AudioFilePath);

            if (SendAudio)
                call.RtpEngine.StartAudio();
        }

        private void Forward(Object request)
        {
            TextLog("MSG: Call being forwarded to " + ForwardTo + " ...");
            
            Request incommingCall = (Request)request;
            Response response = Client.SendResponseCallIsBeingForwarded(incommingCall);
            Client.SendResponseOK(incommingCall);

            Dialog dialog = Client.GetDialog(response);
            string referTo = "sip:" + ForwardTo + "@" + Client.Domain;
            Client.Refer(dialog, referTo);
            Client.Bye(response);
        }

        private void CreateLogFile()
        {
            if (logCreated)
                return;

            if (!SipLogging && !PlainLogging)
                return;

            Thread.Sleep(1000);

            string workingDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string fileType = ".txt";
            Directory.CreateDirectory(workingDirectory + @"\Logs");
            FilePath = workingDirectory + @"\Logs\SIP Tester Log - " + DateTime.UtcNow.ToString("dd MMM yyyy HH.mm.ss.fff", CultureInfo.InvariantCulture) + fileType;
            
            while (File.Exists(FilePath))
            {
                FilePath = workingDirectory + @"\Logs\SIP Tester Log - " + DateTime.UtcNow.ToString("dd MMM yyyy HH.mm.ss.fff", CultureInfo.InvariantCulture) + fileType;
            }

            using (StreamWriter sw = new StreamWriter(FilePath, true))
            {
                sw.Write("SIP Tester Log : ");
                sw.WriteLine("{0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString());
                sw.WriteLine("---------------------------------------");
            }
            logCreated = true;
        }

        private void WriteScreenLog(string message)
        {
            //Add message to MessageLog
            if (Application.Current == null
                || Thread.CurrentThread.ManagedThreadId == Application.Current.Dispatcher.Thread.ManagedThreadId)
                MessageLog.Add(new TraceMessage(message));
            else
            {
                Application.Current.Dispatcher.BeginInvoke(new saveMessage(WriteScreenLog), new Object[] { message });
                return;
            }
        }

        private void WriteFileLog(string message)
        {
            lock (logFileMessages)
            {
                logFileMessages.Add(message);
            }
        }

        private void UpdateOnCall()
        {
            if (currentCalls.Count > 0)
                OnCall = true;
            else
                OnCall = false;
        }

        private void UpdateIncomingInvites()
        {
            if (incomingInvites.Count > 0)
                UnansweredCall = true;
            else
                UnansweredCall = false;
        }
        #endregion
    }
}
