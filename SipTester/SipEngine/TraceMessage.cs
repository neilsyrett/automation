﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regus.SipTester
{
    public class TraceMessage
    {
        public string Message { get; set; }

        public TraceMessage(string Message)
        {
            this.Message = Message;
        }

        public override string ToString()
        {
            return Message;
        }
    }
}
