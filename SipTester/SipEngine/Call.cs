﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Independentsoft.Sip;
using Independentsoft.Sip.Sdp;

namespace Regus.SipTester
{
    public class Call
    {
        public bool Incoming { get; private set; }
        public string ToAddress { get; private set; }
        public RtpEngine RtpEngine { get; set; }
        public SessionDescription SessionDescription { get; private set; }

        private RequestResponse requestResponse;
        public RequestResponse RequestResponse
        {
            get
            {
                return requestResponse;
            }
            set
            {
                requestResponse = value;
                SessionDescription = value.Response.SessionDescription;
            }
        }

        public bool OnHold { get; set; }
        //{
        //    get
        //    {
        //        foreach (Independentsoft.Sip.Sdp.Attribute attribute in SessionDescription.Media[0].Attributes)
        //        {
        //            if (attribute.Name == "sendonly")
        //                return true;
        //        }
        //            return false;
        //    }
        //}

        public string CallID
        {
            get
            {
                if (RequestResponse != null && RequestResponse.Response != null)
                    return RequestResponse.Response.CallID;
                else
                    return null;
            }
        }

        private bool isConnected;
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
            set
            {
                isConnected = value;
                if (!isConnected && RtpEngine != null)
                    RtpEngine.StopAudio();
            }
        }

        public void SetRequestResponse(Request request, Response response)
        {
            RequestResponse = new RequestResponse(request, response);
        }

        public Call(string toAddress)
        {
            OnHold = false;
            Incoming = false;
            ToAddress = toAddress;
        }

        public Call(Request request, Response response)
        {
            OnHold = false;
            Incoming = true;
            requestResponse = new RequestResponse(request, response);
            SessionDescription = RequestResponse.Request.SessionDescription;
        }
    }
}
