﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Web;

namespace Regus.SipTester
{
    public class CallRecord : IDisposable
    {
        public int      ComEventID      { get; set; }
        public string   From            { get; set; }
        public DateTime TimeStarted     { get; set; }
        public double   CallTime        { get; set; }
        public double   BillTime        { get; set; }
        public string   To              { get; set; }
        public string   CallType        { get; set; }
        public string   AccountCode     { get; set; }
        public string   Extension       { get; set; }
        public int      CommWizardID    { get; set; }
        public int      BranchID        { get; set; }
        public string   CountryCode     { get; set; }
        public string   UniqueID        { get; set; }
        public string   CDR             { get; set; }
        public string   Status          { get; set; }
        public int      ExportLogID     { get; set; }
        public string   ExportStatus    { get; set; }
        public DateTime CreatedDate     { get; set; }
        public string   CallID          { get; set; }

        void IDisposable.Dispose() {  }
    }
}
