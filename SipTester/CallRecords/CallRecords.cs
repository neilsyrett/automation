﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Globalization;
using MySql.Data.MySqlClient;

namespace Regus.SipTester
{
    public static class CallRecords
    {
        public static string FilePath { get; set; }
        public static string Server { get; set; }
        public static string Database { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        
        private static MySqlConnection connection;

        public static bool GetCallRecords(DateTime fromTime, DateTime toTime, Collection<CallRecord> calls)
        {
            if (!Connect())
                return false;

            calls.Clear();

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT * FROM ComEvents WHERE TimeStarted BETWEEN '{0}' AND '{1}' ORDER BY UniqueID Asc;", fromTime.ToString("yyyy-MM-dd HH:mm:ss"), toTime.ToString("yyyy-MM-dd HH:mm:ss"));
            MySqlDataReader reader = null;

            try
            {
                MySqlCommand cmd = new MySqlCommand(builder.ToString(), connection);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    CallRecord callRecord = new CallRecord();
                    DatabaseToCallRecord(reader, callRecord);
                    calls.Add(callRecord);
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Disconnect();
            }

            if (calls.Count > 0)
                return true;
            else
                return false;
        }

        public static Collection<CallRecord> GetCallRecords(string callID)
        {
            Collection<CallRecord> calls = new Collection<CallRecord>();

            if (!Connect())
                return calls;

            try
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendFormat("SELECT * FROM ComEvents WHERE CDR LIKE '%{0}%' ORDER BY UniqueID  Asc", callID);
                MySqlCommand command = new MySqlCommand(builder.ToString(), connection);
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CallRecord callRecord = new CallRecord();
                        DatabaseToCallRecord(reader, callRecord);
                        calls.Add(callRecord);
                    }
                }
            }
            catch (MySqlException)
            {
                throw;
            }
            
            Disconnect();
            return calls;
        }

        public static void WriteCallRecord(CallRecord call)
        {
            if (call != null)
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    CreateFile();
                }

                using (StreamWriter sw = new StreamWriter(FilePath, true))
                {
                    sw.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"", call.ComEventID, call.From, call.TimeStarted, call.CallTime, call.BillTime, call.To, call.CallType, call.AccountCode, call.Extension, call.CommWizardID, call.BranchID, call.CountryCode, call.UniqueID, call.CDR.Replace("\"", "\"\""), call.Status, call.ExportLogID, call.ExportStatus, call.CreatedDate);
                }
            }
        }

        public static void WriteCallRecords(Collection<CallRecord> calls)
        {
            if (calls.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(FilePath))
                {
                    CreateFile();
                }

                foreach (CallRecord call in calls)
                {
                    using (StreamWriter sw = new StreamWriter(FilePath, true))
                    {
                        sw.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"", call.ComEventID, call.From, call.TimeStarted, call.CallTime, call.BillTime, call.To, call.CallType, call.AccountCode, call.Extension, call.CommWizardID, call.BranchID, call.CountryCode, call.UniqueID, call.CDR.Replace("\"", "\"\"") , call.Status, call.ExportLogID, call.ExportStatus, call.CreatedDate);
                    }
                }
            }
        }

        private static bool Connect()
        {
            if (connection != null && connection.State == ConnectionState.Open)
                return true;

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Server={0}; Database={1}; Uid={2}; Pwd={3};", Server, Database, Username, Password);
            connection = new MySqlConnection(builder.ToString());

            try
            {
                connection.Open();
                //MySqlCommand cmd = new MySqlCommand("set net_write_timeout=99999; set net_read_timeout=99999", connection); cmd.ExecuteNonQuery();
                //cmd.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                throw;
            }

            return true;
        }

        private static void Disconnect()
        {
            if (connection == null || connection.State != ConnectionState.Open)
                return;

            try
            {
                connection.Close();
            }
            catch (MySqlException)
            {
                throw;
            }
        }

        private static void CreateFile()
        {
            string workingDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Directory.CreateDirectory(workingDirectory + @"\Call Records");
            FilePath = @"\Call Records\SIP Tester Call Records - " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + ".csv";
            FilePath = FilePath.Replace(':', '.');
            FilePath = workingDirectory + FilePath;

            using (StreamWriter sw = new StreamWriter(FilePath))
            {
                sw.WriteLine("\"ComEventID\",\"From\",\"TimeStarted\",\"CallTime\",\"BillTime\",\"To\",\"CallType\",\"AccountCode\",\"Extension\",\"CommWizardID\",\"BranchID\",\"CountryCode\",\"UniqueID\",\"CDR\",\"Status\",\"ExportLogID\",\"ExportStatus\",\"CreatedDate\"");
            }
        }

        private static void DatabaseToCallRecord(MySqlDataReader reader, CallRecord callRecord)
        {
            callRecord.ComEventID = Convert.ToInt32(reader.GetString("ComEventID"));
            callRecord.From = reader.GetString("From");
            callRecord.TimeStarted = Convert.ToDateTime(reader.GetString("TimeStarted"));
            callRecord.CallTime = Convert.ToDouble(reader.GetString("CallTime"));
            callRecord.BillTime = Convert.ToDouble(reader.GetString("BillTime"));
            callRecord.To = reader.GetString("To");
            callRecord.CallType = reader.GetString("CallType");
            callRecord.AccountCode = reader.GetString("AccountCode");
            callRecord.Extension = reader.GetString("Extension");
            callRecord.CommWizardID = Convert.ToInt32(reader.GetString("CommWizardID"));
            callRecord.BranchID = Convert.ToInt32(reader.GetString("BranchID"));
            callRecord.CountryCode = reader.GetString("CountryCode");
            callRecord.UniqueID = reader.GetString("UniqueID");
            callRecord.Status = reader.GetString("Status");
            callRecord.ExportLogID = Convert.ToInt32(reader.GetString("ExportLogID"));
            callRecord.ExportStatus = reader.GetString("ExportStatus");
            callRecord.CreatedDate = Convert.ToDateTime(reader.GetString("CreatedDate"));
            callRecord.CDR = reader.GetString("CDR");

            int start = callRecord.CDR.IndexOf("sipid=");
            if (start > 0)
            {
                start += 6;
                int end = callRecord.CDR.IndexOf(',', start);
                if (end > 0)
                {
                    callRecord.CallID = callRecord.CDR.Substring(start, end - start);
                }
            }
        }
    }
}
