﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Routrek.SSHC;

namespace Regus.SipTester
{
    public class SshEngine : ISSHConnectionEventReceiver, ISSHChannelEventReceiver
    {
        public SSHConnection Connection;
        public SSHChannel Channel;
        public SSHConnectionInfo ConnectionInformation;

        public event ReceivedData Notify;
        public delegate void ReceivedData(object sender, String s);

        public bool Connect(string hostname, string user, string pass)
        {
            SSHConnectionParameter parameters = new SSHConnectionParameter();
            parameters.UserName = user;
            parameters.Password = pass;
            parameters.Protocol = SSHProtocol.SSH2;
            parameters.AuthenticationType = AuthenticationType.Password;

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ip;

            try
            {
                ip = IPAddress.Parse(hostname);
            }
            catch
            {
                Notify(this, "Couldn't resolve HostName" + Environment.NewLine);
                socket.Close();
                return false;
            }

            try
            {
                socket.Connect(new IPEndPoint(ip, 22));
            }
            catch (Exception e)
            {
                Notify(this, e.Message + Environment.NewLine);
                return false;
            }

            try
            {
                //Open the connection
                Connection = SSHConnection.Connect(parameters, this, socket);

                //Open the channel to transmit data
                Channel = Connection.OpenShell(this);

                //Get connection information
                ConnectionInformation = Connection.ConnectionInfo;
            }
            catch (Exception e)
            {
                Notify(this, e.Message + Environment.NewLine);
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            if (Connection != null && !Connection.IsClosed)
            {
                Connection.Disconnect("");
                Connection = null;
                ConnectionInformation = null;
            }

            if (Channel != null)
            {
                Channel.Close();
                Channel = null;
            }
        }

        public void Transfer(string command)
        {
            if (Connection != null && !Connection.IsClosed)
            {
                byte[] commandBytes = Encoding.ASCII.GetBytes(command + Environment.NewLine);
                Channel.Transmit(commandBytes, 0, commandBytes.Length);
            }
        }

        //This is the function called every time we receive data through the connection
        //so we notify the main app that we received and event and pass the received string
        public void OnData(byte[] data, int offset, int length)
        {
            Notify(this, Encoding.ASCII.GetString(data, offset, length));
        }

        public void OnDebugMessage(bool always_display, byte[] data)
        { }

        public void OnIgnoreMessage(byte[] data)
        { }

        public void OnAuthenticationPrompt(string[] msg)
        { }

        public void OnError(Exception error, string msg)
        { }

        public void OnChannelClosed()
        {
            Connection.Disconnect("");
        }

        public void OnChannelEOF()
        {
            Channel.Close();
        }

        public void OnExtendedData(int type, byte[] data)
        { }

        public void OnConnectionClosed()
        { }

        public void OnUnknownMessage(byte type, byte[] data)
        { }

        public void OnChannelReady()
        { }

        public void OnChannelError(Exception error, string msg)
        { }

        public void OnMiscPacket(byte type, byte[] data, int offset, int length)
        { }

        public PortForwardingCheckResult CheckPortForwardingRequest(string host, int port, string originatorHost, int originatorPort)
        {
            Routrek.SSHC.PortForwardingCheckResult result = new PortForwardingCheckResult();
            result.allowed = true;
            result.channel = this;
            return result;
        }

        public void EstablishPortforwarding(ISSHChannelEventReceiver receiver, SSHChannel channel)
        {
            Channel = channel;
        }
    }
}
