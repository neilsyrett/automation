﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;

namespace Regus.SipTester
{
    public static class NetworkEngine
    {
        public static PhysicalAddress GetMacAddress(string networkAdaptorID)
        {
            NetworkInterface networkInterface = GetNetworkInterface(networkAdaptorID);

            if (networkInterface != null)
            {
                return networkInterface.GetPhysicalAddress();
            }
            return null;
        }

        public static IPEndPoint GetLocalEndPoint(string networkAdaptorID, int port)
        {
            NetworkInterface networkInterface = GetNetworkInterface(networkAdaptorID);

            if (networkInterface != null)
            {
                foreach (UnicastIPAddressInformation ip in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        return new IPEndPoint(ip.Address, port);
                    }
                }
            }
            return null;
        }

        public static IPAddress GetIPAddress(string networkAdaptorID)
        {
            NetworkInterface networkInterface = GetNetworkInterface(networkAdaptorID);

            if (networkInterface != null)
            {
                foreach (UnicastIPAddressInformation ip in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        return ip.Address;
                    }
                }
            }
            return null;
        }

        public static NetworkInterface GetNetworkInterface(string networkAdaptorID)
        {
            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (networkInterface.Id == networkAdaptorID)
                {
                    return networkInterface;
                }
            }
            return null;
        }

        public static int GetNextPort(IPAddress ipAddress)
        {
            TcpListener listener = new TcpListener(ipAddress, 0);
            listener.Start();
            int port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }
    }
}
