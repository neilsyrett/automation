﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.ComponentModel;

namespace Regus.SipTester
{
    class Program
    {
        static SipEngine sipEngine;
        static string stubText = "Please specify...";
        static string domain;
        static string username;

        public static void Main(string[] args)
        {
            string password;
            string forwardTo = null;
            int? answerDelay = null;
            char closeOnHangup = ' ';

            if (args.Length > 6)
            {
                Properties.Settings.Default.NetworkAdaptorID = args[6];
                Properties.Settings.Default.Save();
            }

            if (String.IsNullOrWhiteSpace(Properties.Settings.Default.NetworkAdaptorID)
                || Properties.Settings.Default.NetworkAdaptorID == stubText)
            {
                Console.WriteLine("NetworkAdaptorID user settings not specified.  Opening configuration file:\r\n\r\n{0}\r\n", ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath);

                Properties.Settings.Default.NetworkAdaptorID = stubText;
                Properties.Settings.Default.Save();

                Process process = new Process();
                process.StartInfo.FileName = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;

                try
                {
                    process.Start();
                }
                catch (Win32Exception ex)
                {
                    if (ex.ErrorCode == -2147467259)
                    {
                        process.StartInfo.Arguments = process.StartInfo.FileName;
                        process.StartInfo.FileName = "notepad.exe";
                        process.Start();
                    }
                }

                try
                {
                    process.WaitForExit();
                }
                catch (InvalidOperationException)
                {
                    return;
                }
                
                Properties.Settings.Default.Reload();
            }

            //Read in command line arguments
            if (args.Length < 1)
            {
                Console.WriteLine("Enter the domain name or IP address of the SIP server: ");
                domain = Console.ReadLine();
            }
            else
            {
                domain = args[0];
            }

            if (args.Length < 2)
            {
                Console.WriteLine("Enter username: ");
                username = Console.ReadLine();
            }
            else
            {
                username = args[1];
            }

            if (args.Length < 3)
            {
                Console.WriteLine("Enter password: ");
                password = Console.ReadLine();
            }
            else
            {
                password = args[2];
            }

            if (args.Length < 4)
            {
                do
                {
                    Console.WriteLine("Enter a number of milliseconds (up to {0}) to wait before answering each call\nEnter 0 to use a random wait time for each call: ", Int32.MaxValue);
                    try
                    {
                        answerDelay = Convert.ToInt32(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Wait time must be numeric.\n");
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Wait time specified is too large.\n");
                    }
                } while (answerDelay == null);
            }
            else
            {
                try
                {
                    answerDelay = Convert.ToInt32(args[3]);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Wait time must be numeric.");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Wait time specified is too large.");
                }
            }

            if (args.Length < 5)
            {
                Console.WriteLine("Close Parrot after call has ended? Y/N: ");
                do
                {
                    try
                    {
                        closeOnHangup = Convert.ToChar(Console.ReadLine());
                    }
                    catch (Exception) { }
                    Console.WriteLine("Please enter Y or N.");

                } while (closeOnHangup != 'y' && closeOnHangup != 'Y' && closeOnHangup != 'n' && closeOnHangup != 'N');
            }
            else
            {
                closeOnHangup = args[4][0];
            }

            if (args.Length < 6)
            {
                Console.WriteLine("Enable call forwading? Forward to number: ");
                forwardTo = Console.ReadLine();
            }
            else
            {
                forwardTo = args[5];
            }

            if (String.IsNullOrWhiteSpace(forwardTo) || forwardTo == "0")
                forwardTo = null;

            Console.WriteLine();

            sipEngine = new SipEngine(Properties.Settings.Default.PlainLogging, Properties.Settings.Default.SipLogging);

            if (answerDelay != null)
                sipEngine.AnswerDelayMS = (int)answerDelay;

            if (forwardTo != null)
                sipEngine.ForwardTo = forwardTo;

            if(closeOnHangup == 'y' || closeOnHangup == 'Y')
                sipEngine.DisconnectAfterCall = true;

            sipEngine.MessageLog.CollectionChanged += ContentCollectionChanged;
            sipEngine.PropertyChanged += sipEngine_PropertyChanged;
            sipEngine.AudioFilePath = Properties.Settings.Default.AudioFilesLocation;
            sipEngine.Connect(domain, username, password, Properties.Settings.Default.NetworkAdaptorID, 0);

            //Continue until user presses a key
            Console.WriteLine("Press any key to exit.");
            Console.Read();

            sipEngine.Disconnect();
        }

        private static void sipEngine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsConnected" && sipEngine.IsConnected == false)
                Environment.Exit(0);
        }

        #region Private Methods
        private static void ContentCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sipEngine.MessageLog.Count > 0)
                Console.WriteLine(sipEngine.MessageLog[sipEngine.MessageLog.Count - 1]);
        }
        #endregion
    }
}

