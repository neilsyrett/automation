﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regus.SipTester
{
    public class UserSetting
    {
        public string Name { get; internal set; }
        public string Value { get; internal set; }

        public UserSetting(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
}
