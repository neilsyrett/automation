﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Timers;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Net;
using Regus.SipTester.CommWizardService;

namespace Regus.SipTester
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private OzCommWizardServicesPortTypeClient webClient;
        public event PropertyChangedEventHandler PropertyChanged;
        public SipEngine SipEngine { get; private set; }

        DateTime startTime;

        private string callTime;
        public string CallTime
        {
            get
            {
                return callTime;
            }
            private set
            {
                this.callTime = value;
                NotifyPropertyChanged("CallTime");
            }
        }

        System.Timers.Timer timer;

        public MainWindow()
        {
            InitializeComponent();
            ConfigureSipEngine();
            CallTime = new DateTime().ToString("HH:mm:ss");
            this.DataContext = this;

            //Check for command line parameters
            string[] args = Environment.GetCommandLineArgs();
            
            if (args.Length > 1)
                TextBoxUsername.Text = args[1];

            if (args.Length > 2)
                TextBoxPassword.Text = args[2];

            if (args.Length > 3)
                TextBoxDialDisplay.Text = args[3];

            SipEngine.MessageLog.CollectionChanged += ContentCollectionChanged;
            SipEngine.PropertyChanged += CallStatusChange;

            if (args.Length > 2)
            {
                if (Connect(true))
                {
                    Call();
                }
            }
        }

        private void ConfigureSipEngine()
        {
            if (SipEngine == null)
            {
                SipEngine = new SipEngine(Properties.Settings.Default.PlainLogging, Properties.Settings.Default.SipLogging);
            }
            else
            {
                SipEngine.PlainLogging = Properties.Settings.Default.PlainLogging;
                SipEngine.SipLogging = Properties.Settings.Default.SipLogging;
            }

            SipEngine.SessionVersion = Properties.Settings.Default.SessionVersion;
            SipEngine.SessionName = Properties.Settings.Default.SessionName;
            SipEngine.DialToneDelayMS = Properties.Settings.Default.DialToneDelayMilliseconds;
            SipEngine.AudioFilePath = Properties.Settings.Default.AudioFilesLocation;
            SipEngine.AudioOutputDevice = Properties.Settings.Default.AudioOutputDevice;
        }

        private bool Connect(bool waitToConnect = false)
        {
            SipEngine.Connect(Properties.Settings.Default.ServerIP, TextBoxUsername.Text, TextBoxPassword.Text, Properties.Settings.Default.NetworkAdaptorID, 0, waitToConnect);
            return SipEngine.IsConnected;
        }

        private void Call()
        {
            if (!String.IsNullOrWhiteSpace(TextBoxDialDisplay.Text))
            {
                SipEngine.Call(TextBoxDialDisplay.Text, false);
            }
        }

        private void ContentCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ListBoxMessageLog.Items.Count > 0)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() =>
                {
                    ListBoxMessageLog.SelectedItem = ListBoxMessageLog.Items[ListBoxMessageLog.Items.Count - 1];
                    ListBoxMessageLog.ScrollIntoView(ListBoxMessageLog.Items[ListBoxMessageLog.Items.Count - 1]);
                }));
            }
        }


        private void CallStatusChange(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName=="OnCall")
            {
                if (((SipEngine)sender).OnCall)
                {
                    CallTime = new DateTime().ToString("HH:mm:ss");
                    startTime = DateTime.Now;
                    timer = new System.Timers.Timer();
                    timer.Elapsed += new ElapsedEventHandler(TimerTick);
                    timer.Interval = 100;
                    timer.Start();
                }
                else
                    timer.Stop();
            }
        }

        private void NotifyPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void TimerTick(Object sender, EventArgs e)
        {
            CallTime = (DateTime.Now - startTime).ToString(@"hh\:mm\:ss"); 
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            Connect();
        }

        private void ButtonDisconnect_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.Disconnect();
        }

        private void Button_Click_Digit(object sender, RoutedEventArgs e)
        {
            TextBoxDialDisplay.Text += ((System.Windows.Controls.Button)sender).Content.ToString();
            SipEngine.SendTone((((System.Windows.Controls.Button)sender).Content.ToString()[0]));
        }

        private void ButtonSettings_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.ShowDialog();
            settingsWindow = null;
            SipEngine.Disconnect();
            ConfigureSipEngine();
        }

        private void ButtonCompare_Click(object sender, RoutedEventArgs e)
        {
            CompareWindow compareWindow = new CompareWindow();
            compareWindow.ShowDialog();
            compareWindow = null;
        }

        private void ButtonImport_Click(object sender, RoutedEventArgs e)
        {
            ImportWindow importWindow = new ImportWindow();
            importWindow.ShowDialog();
            importWindow = null;
        }

        private void ButtonSSH_Click(object sender, RoutedEventArgs e)
        {
            SshWindow sshWindow = new SshWindow();
            sshWindow.Show();
        }
        private void ButtonConfig_Click(object sender, RoutedEventArgs e)
        {
            ConfigWindow configWindow = new ConfigWindow();
            configWindow.Show();
        }

        private void ButtonCall_Click(object sender, RoutedEventArgs e)
        {
            Call();
        }

        private void ButtonAnswer_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.AnswerNextCall();
        }

        private void ButtonHangup_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.EndCall();
        }

        private void ButtonHold_Click(object sender, RoutedEventArgs e)
        {
            if (!SipEngine.LastCall.OnHold)
                SipEngine.HoldCall(SipEngine.LastCall);
            else
                SipEngine.ResumeHeldCall(SipEngine.LastCall);
        }

        private void ButtonTransfer_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.TransferCall(TextBoxDialDisplay.Text);
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.WindowHeight = this.Height;
            Properties.Settings.Default.WindowWidth = this.Width;
            Properties.Settings.Default.Save();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            SipEngine.Disconnect();
            //Application.Current.Shutdown();
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.ClearLog();
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(SipEngine.FilePath))
            {
                Process.Start(SipEngine.FilePath);
            }
        }

        private void TextBoxDialDisplay_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void TextBoxDialDisplay_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!IsNumeric(e.Text))
                //SipEngine.SendTone(Convert.ToChar(e.Text));
            //else
                e.Handled = true;
        }

        private bool IsNumeric(string str)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^[0-9#*]+$");
            return regex.IsMatch(str);
        }

        private void TextBoxUsername_GotFocus(object sender, RoutedEventArgs e)
        {
            if (((TextBox)sender).Text == "Extension")
                ((TextBox)sender).Text = String.Empty;
        }

        private void TextBoxPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (((TextBox)sender).Text == "Password")
                ((TextBox)sender).Text = String.Empty;
        }

        private void TextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SipEngine.Connect(Properties.Settings.Default.ServerIP, TextBoxUsername.Text, TextBoxPassword.Text, Properties.Settings.Default.NetworkAdaptorID, 0);
        }

        private void TextBoxDialDisplay_KeyDown(object sender, KeyEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(TextBoxDialDisplay.Text) && e.Key == Key.Enter && SipEngine.CanCall == true)
                SipEngine.Call(TextBoxDialDisplay.Text, false);
        }

        private void ButtonUpdateAll_Click(object sender, RoutedEventArgs e)
        {
            //Call CallStream webservice to progate Extension details
            if (webClient == null)
                webClient = new OzCommWizardServicesPortTypeClient("OzCommWizardServicesPort", "http://" + Properties.Settings.Default.ServerIP + "/commwizard/CMLocalServices/services.php");

            webClient.com_update_extension(new CommWizardService.sessioninfo(), "0");

            MessageBox.Show("All extensions updated.", "Update Complete");
        }
    }
}
