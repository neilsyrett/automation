﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Regus.SipTester
{
    public partial class SshWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string ServerIP
        {
            get
            {
                return Properties.Settings.Default.ServerIP;
            }
        }

        public string Username
        {
            get
            {
                return Properties.Settings.Default.AsteriskUsername;
            }
        }

        public string Password
        {
            get
            {
                return Properties.Settings.Default.AsteriskPassword;
            }
        }

        private string output;
        public string Output
        {
            get
            {
                return output;
            }
            private set
            {
                output = value;
                NotifyPropertyChanged("Output");
            }
        }

        private SshEngine sshEngine;

        public SshWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void TextBoxCommand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (sshEngine != null)
                {
                    sshEngine.Transfer(TextBoxCommand.Text);
                    TextBoxCommand.Text = String.Empty;
                }
            }
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            Output = string.Empty;

            if (sshEngine != null)
                sshEngine.Disconnect();
                
            sshEngine = new SshEngine();              
            sshEngine.Notify += new SshEngine.ReceivedData(printResult);
            sshEngine.Connect(TextBoxServerIP.Text, TextBoxUsername.Text, TextBoxPassword.Text);
        }

        private void printResult(object sender, string s)
        {
            Output += s;
        }

        private void NotifyPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void TextBoxOutput_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBoxOutput.ScrollToEnd();
        }

        private void SshWindow_Closing(object sender, CancelEventArgs e)
        {
            if (sshEngine != null)
                sshEngine.Disconnect();
        }
    }
}
