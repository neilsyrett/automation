﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using Regus.SipTester.Properties;
using LumiSoft.Net.Media;
using System.ComponentModel;

namespace Regus.SipTester
{
    public partial class SettingsWindow : Window
    {
        public ObservableCollection<UserSetting> UserSettings { get; private set; }
        public ObservableCollection<AudioOutDevice> AudioOutputDevices { get; private set; }
        public ObservableCollection<NetworkInterface> NetworkInterfaces { get; private set; }
        private bool reloadOnExit = false;

        public SettingsWindow()
        {
            try
            {
                InitializeComponent();
                this.DataContext = this;
                LoadNetworkAdaptors();
                LoadOutputAudioDevices();
                LoadUserSettings();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void ButtonEditSettings_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            Process process = new Process();
            process.StartInfo.FileName = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;

            try
            {
                process.Start();
            }
            catch (Win32Exception ex)
            {
                if (ex.ErrorCode == -2147467259)
                {
                    process.StartInfo.Arguments = process.StartInfo.FileName;
                    process.StartInfo.FileName = "notepad.exe";
                    process.Start();
                }
            }

            try
            {
                process.WaitForExit();
            }
            catch (InvalidOperationException)
            {
                reloadOnExit = true;
                return;
            }

            Properties.Settings.Default.Reload();
            LoadUserSettings();
            DataGridSettings.GetBindingExpression(System.Windows.Controls.DataGrid.ItemsSourceProperty).UpdateTarget();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (reloadOnExit)
                Properties.Settings.Default.Reload();
        }

        private void ComboBoxNetworkAdaptor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxNetworkAdaptor.SelectedValue != null)
                Properties.Settings.Default.NetworkAdaptorID = ComboBoxNetworkAdaptor.SelectedValue.ToString();
        }

        private void ComboBoxOutputAudioDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxOutputAudioDevice.SelectedValue != null)
                Properties.Settings.Default.AudioOutputDevice = ComboBoxOutputAudioDevice.SelectedValue.ToString();
        }

        private void LoadNetworkAdaptors()
        {
            NetworkInterfaces = new ObservableCollection<NetworkInterface>();
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface netInterface in networkInterfaces)
            {
                NetworkInterfaces.Add(netInterface);
            }

            if (NetworkInterfaces.Count > 0)
            {
                if (!String.IsNullOrWhiteSpace(Properties.Settings.Default.NetworkAdaptorID))
                {
                    ComboBoxNetworkAdaptor.SelectedValue = Properties.Settings.Default.NetworkAdaptorID;
                }
                else
                {
                    ComboBoxNetworkAdaptor.SelectedIndex = 0;
                }
            }
        }

        private void LoadOutputAudioDevices()
        {
            AudioOutputDevices = new ObservableCollection<AudioOutDevice>();
            AudioOutDevice[] audioDevices = AudioOut.Devices;
            foreach (AudioOutDevice audioDevice in audioDevices)
            {
                AudioOutputDevices.Add(audioDevice);
            }

            if (AudioOutputDevices.Count > 0)
            {
                if (!String.IsNullOrWhiteSpace(Properties.Settings.Default.AudioOutputDevice))
                {
                    ComboBoxOutputAudioDevice.SelectedValue = Properties.Settings.Default.AudioOutputDevice;
                }
                else
                {
                    ComboBoxOutputAudioDevice.SelectedIndex = 0;
                }
            }

            //ManagementObjectSearcher mo = new ManagementObjectSearcher("SELECT * FROM Win32_SoundDevice");
            //foreach (ManagementObject soundDevice in mo.Get())
            //{
            //    AudioOutputDevice device = new AudioOutputDevice(soundDevice.GetPropertyValue("Name").ToString(), soundDevice.GetPropertyValue("DeviceId").ToString());
            //    AudioOutputDevices.Add(device);

            //    string details = "";
            //    foreach (PropertyData property in soundDevice.Properties)
            //    {
            //        details += String.Format("{0}:{1}\r\n", property.Name, property.Value);
            //    }
            //    MessageBox.Show(details);
            //}
            //ComboBoxOutputAudioDevice.SelectedValue = Properties.Settings.Default.AudioOutputDeviceId;
        }

        private void LoadUserSettings()
        {
            UserSettings = new ObservableCollection<UserSetting>();
            foreach (PropertyInfo propertyInfo in typeof(Settings).GetProperties())
            {
                try
                {
                    if (propertyInfo.CanWrite
                        && String.Compare(propertyInfo.Name, 0, "Window", 0, 5) < 0
                        && String.Compare(propertyInfo.Name, "SettingsKey") != 0)
                    {
                        UserSetting uS = new UserSetting(propertyInfo.Name, propertyInfo.GetValue(Settings.Default, null).ToString());
                        UserSettings.Add(uS);
                    }
                }
                catch { }
            }
            DataGridSettings.Focus();
        }
     }
}
