﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Collections.Specialized;
using System.Windows.Threading;
using System.ComponentModel;

namespace Regus.SipTester
{
    public partial class ImportWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public SipEngine SipEngine { get; private set; }
        public bool IsExtractRequired { get; set; }

        private bool isDisconnectRequired;
        public bool IsDisconnectRequired
        {
            get
            {
                return isDisconnectRequired;
            }
            set
            {
                this.isDisconnectRequired = value;
                NotifyPropertyChanged("IsDisconnectRequired");
            }
        }

        private bool isExportComplete;
        public bool IsExportComplete
        {
            get
            {
                return isExportComplete;
            }
            private set
            {
                this.isExportComplete = value;
                NotifyPropertyChanged("IsExportComplete");
            }
        }
        
        private double progress;
        public double Progress
        {
            get
            {
                return progress;
            }
            private set
            {
                this.progress = value;
                NotifyPropertyChanged("Progress");
            }
        }

        private bool inProgress;
        public bool InProgress
        {
            get
            {
                return inProgress;
            }
            private set
            {
                this.inProgress = value;
                NotifyPropertyChanged("InProgress");
            }
        }

        string filePath;
        DateTime startTime;
        DateTime endTime;

        public ImportWindow()
        {
            IsExtractRequired = false;
            IsDisconnectRequired = false;
            IsExportComplete = false;
            InProgress = false;
            Progress = 0;
            ConfigureSipEngine();
            InitializeComponent();
            this.DataContext = this;
            SipEngine.MessageLog.CollectionChanged += ContentCollectionChanged;
        }

        private void ConfigureSipEngine()
        {
            SipEngine = null;
            SipEngine = new SipEngine(Properties.Settings.Default.PlainLogging, Properties.Settings.Default.SipLogging);
            SipEngine.SessionVersion = Properties.Settings.Default.SessionVersion;
            SipEngine.SessionName = Properties.Settings.Default.SessionName;
            SipEngine.DialToneDelayMS = Properties.Settings.Default.DialToneDelayMilliseconds;
            SipEngine.AudioFilePath = Properties.Settings.Default.AudioFilesLocation;
            SipEngine.AudioOutputDevice = Properties.Settings.Default.AudioOutputDevice;
        }

        private void Import()
        {
            InProgress = true;
            int lineCount = 0;
            int callCount = 0;
            startTime = DateTime.Now;

            try
            {
                // Create an instance of StreamReader to read from a file. 
                // The using statement also closes the StreamReader. 
                using (StreamReader sr = new StreamReader(filePath))
                {
                    // Read lines from the file until the end of the file is reached. 
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        //Ignore blank lines in file
                        if (String.IsNullOrWhiteSpace(line))
                            continue;

                        lineCount++;
                        if (Process(line.Split(','), IsDisconnectRequired))
                            callCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error was encountered while reading the file: \r\n" + ex.Message);
                return;
            }

            endTime = DateTime.Now;
            SipEngine.TextLog("MSG: Lines read from file: " + lineCount + " - calls made: " + callCount);

            if (lineCount > 0 && IsDisconnectRequired && IsExtractRequired)
            {
                GetCallRecords();
            }
            else
            {
                InProgress = false;
                Progress = 100;
            }
        }

        private bool Process(string[] data, bool disconnectAfterCall)
        {
            if (disconnectAfterCall)
                return Process(data);

            if (!SipEngine.IsConnected)
                SipEngine.Connect(data[1], data[2], data[3], Properties.Settings.Default.NetworkAdaptorID, 0, true);
            
            if (SipEngine.IsConnected)
            {
                SipEngine.Call(data[4], false);
            }
            return true;
        }

        private bool Process(string[] data)
        {
            bool success = false;

            SipEngine.Connect(data[1], data[2], data[3], Properties.Settings.Default.NetworkAdaptorID, 0, true);
            if (SipEngine.IsConnected)
            {
                SipEngine.Call(data[4], true);

                if (SipEngine.LastCall.IsConnected)
                {
                    success = true;

                    for (int x = 5; data.Length > x + 1 && !String.IsNullOrWhiteSpace(data[x]); x += 2)
                    {
                        //Wait for specified period
                        int delay = Convert.ToInt32(data[x]);
                        if (delay > 0)
                        {
                            SipEngine.TextLog("MSG: Sleeping for " + data[x] + "ms...");
                            Thread.Sleep(delay);
                        }
                        //Send dial tones
                        if (!String.IsNullOrWhiteSpace(data[x + 1]))
                            SipEngine.SendTones(data[x + 1]);
                    }
                    SipEngine.EndCall();
                }
                SipEngine.Disconnect();
            }
            return success;
        }

        private void GetCallRecords()
        {
            Thread exportThread = new Thread(ExportCallRecords);
            exportThread.Start();
            SipEngine.TextLog("MSG: Call records are being exported, this may take a few minutes...");
        }

        private void ExportCallRecords()
        {
            InProgress = false;
            Progress = 0;

            Thread progressThread = new Thread(IncrementProgress);
            progressThread.Start();

            CallRecords.Server = Properties.Settings.Default.DatabaseServer;
            CallRecords.Database = Properties.Settings.Default.DatabaseName;
            CallRecords.Username = Properties.Settings.Default.DatabaseUsername;
            CallRecords.Password = Properties.Settings.Default.DatabasePassword;

            Thread.Sleep(Properties.Settings.Default.CallRecordExportDelayMS);
            Collection<CallRecord> callRecords = new Collection<CallRecord>();

            TimeSpan time;

            if (Properties.Settings.Default.CallRecordExportTimezoneHours > 0)
            {
                startTime = startTime.AddHours(Properties.Settings.Default.CallRecordExportTimezoneHours);
                endTime = endTime.AddHours(Properties.Settings.Default.CallRecordExportTimezoneHours);
            }
            else if (Properties.Settings.Default.CallRecordExportTimezoneHours < 0)
            {
                time = new TimeSpan(0, Properties.Settings.Default.CallRecordExportTimezoneHours * -1, 0, 0, 0);
                startTime = startTime.Subtract(time);
                endTime = endTime.Subtract(time);
            }
            
            time = new TimeSpan(0,0,0,0,Properties.Settings.Default.CallRecordExportWindowMS);
            startTime = startTime.Subtract(time);
            endTime = endTime.AddMilliseconds((double)Properties.Settings.Default.CallRecordExportWindowMS);
            
            try
            {
                if (CallRecords.GetCallRecords(startTime, endTime, callRecords))
                    CallRecords.WriteCallRecords(callRecords);
            }
            catch (Exception ex)
            {
                SipEngine.TextLog("MSG: An error was encountered while retreiving the call record from the database: \r\n" + ex.Message);
            }

            progressThread.Abort();
            IsExportComplete = true;
            InProgress = false;
            Progress = 100;

            if (callRecords.Count > 0)
                SipEngine.TextLog("MSG: Call records successfully exported to:\r\n" + CallRecords.FilePath);
            else
                SipEngine.TextLog("MSG: No call records found.");
        }

        private void IncrementProgress()
        {
            int sleepTime = Properties.Settings.Default.CallRecordExportDelayMS / 80;
            while (Progress < 99)
            {
                Progress++;
                Thread.Sleep(sleepTime);
            }
        }

        private void NotifyPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        private void ContentCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ListBoxMessageLog.Items.Count > 0)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() =>
                {
                    ListBoxMessageLog.SelectedItem = ListBoxMessageLog.Items[ListBoxMessageLog.Items.Count - 1];
                    ListBoxMessageLog.ScrollIntoView(ListBoxMessageLog.Items[ListBoxMessageLog.Items.Count - 1]);
                    ListBoxMessageLog.Focus();
                }));
            }
        }

        private void Button_Click_Browse(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "import.csv";
            dlg.DefaultExt = ".csv";
            dlg.Filter = "Comma Separated Files|*.csv";

            // Show open file dialog box 
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                TextBoxFilename.Text = dlg.FileName;
            }
        }

        private void ButtonImport_Click(object sender, RoutedEventArgs e)
        {
            //Perform Import
            filePath = TextBoxFilename.Text;
            TextBoxFilename.Text = String.Empty;

            Thread importThread = new Thread(new ThreadStart(Import));
            importThread.Start();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            //Close Import Window
            SipEngine.Disconnect();
            this.Close();
        }

        private void TextBoxFilename_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(TextBoxFilename.Text))
                ButtonImport.IsEnabled = true;
            else
                ButtonImport.IsEnabled = false;
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            SipEngine.ClearLog();
        }

        private void ButtonOpenLog_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(SipEngine.FilePath))
            {
                System.Diagnostics.Process.Start(SipEngine.FilePath);
            }
        }

        private void ButtonOpenCallRecords_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(CallRecords.FilePath))
            {
                System.Diagnostics.Process.Start(CallRecords.FilePath);
            }
        }
    }
}