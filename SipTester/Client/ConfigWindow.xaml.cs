﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Regus.SipTester
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        public ConfigWindow()
        {
            InitializeComponent();
            if (!String.IsNullOrWhiteSpace(Properties.Settings.Default.NetworkAdaptorID))
                TextBoxMacAddress.Text = NetworkEngine.GetMacAddress(Properties.Settings.Default.NetworkAdaptorID).ToString();
        }

        private void ButtonGetConfig_Click(object sender, RoutedEventArgs e)
        {
            TextBoxOutput.Text = String.Empty;

            if (!String.IsNullOrWhiteSpace(TextBoxMacAddress.Text))
                TextBoxOutput.Text = HandsetEngine.SendConfigurationRequest(Properties.Settings.Default.ServerIP, ComboBoxMakeModel.Text, TextBoxMacAddress.Text);
        }
    }
}
