﻿using Microsoft.XmlDiffPatch;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace Regus.SipTester
{
    /// <summary>
    /// Interaction logic for CompareWindow.xaml
    /// </summary>
    public partial class CompareWindow : Window
    {
        public CompareWindow()
        {
            InitializeComponent();
        }

        private void ButtonBrowse1_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog browseDialog = new Microsoft.Win32.OpenFileDialog();
            browseDialog.FileName = "file1.xml";
            browseDialog.DefaultExt = ".xml";
            browseDialog.Filter = "eXtensible Markup Language|*.xml";

            // Show open file dialog box 
            if (browseDialog.ShowDialog() == true)
                TextBoxFilename1.Text = browseDialog.FileName;
        }

        private void ButtonBrowse2_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog browseDialog = new Microsoft.Win32.OpenFileDialog();
            browseDialog.FileName = "file2.xml";
            browseDialog.DefaultExt = ".xml";
            browseDialog.Filter = "eXtensible Markup Language|*.xml";

            if (browseDialog.ShowDialog() == true)
                TextBoxFilename2.Text = browseDialog.FileName;
        }

        private void ButtonCompare_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(TextBoxFilename1.Text) && !String.IsNullOrWhiteSpace(TextBoxFilename2.Text))
            {
                StringBuilder outputStringBuilder = new StringBuilder();
                XmlDiff xmlDiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnorePrefixes);

                using (XmlTextReader reader1 = new XmlTextReader(TextBoxFilename1.Text))
                using (XmlTextReader reader2 = new XmlTextReader(TextBoxFilename2.Text))
                using (XmlWriter xmlWriter = XmlWriter.Create(outputStringBuilder))
                {
                    if (xmlDiff.Compare(reader1, reader2, xmlWriter))
                        TextBoxDifferences.Text = "No Differences";
                    else
                        TextBoxDifferences.Text = "Differences:\n\r" + outputStringBuilder.ToString();
                }
            }
        }
    }
}
