﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using Tftp.Net;

namespace Regus.SipTester
{
    public static class TftpEngine
    {
        private static AutoResetEvent TransferFinishedEvent = new AutoResetEvent(false);

        public static string Transfer(string hostname, string filename)
        {
            TftpClient client = new TftpClient(new IPEndPoint(IPAddress.Parse(hostname), 69));
            ITftpTransfer transfer = client.Receive(filename);
            transfer.OnFinished += new TftpEventHandler(OnFinshed);
            transfer.OnError += new TftpErrorHandler(OnError);

            //Start the transfer and write the data that we're downloading into a memory stream
            MemoryStream stream = new MemoryStream();
            transfer.Start(stream);

            //Wait for the transfer to finish
            TransferFinishedEvent.WaitOne();
            return stream.ToString();
        }

        private static void OnError(ITftpTransfer transfer, TftpTransferError error)
        {
            TransferFinishedEvent.Set();
        }

        private static void OnFinshed(ITftpTransfer transfer)
        {
            TransferFinishedEvent.Set();
        }
    }
}
