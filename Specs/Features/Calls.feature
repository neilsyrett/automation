﻿@Calls
Feature: Calls
	In order to make telephone calls
	As a CallStream user
	I want to be able to dial out and receive incoming calls

@Smoke
Scenario: Calling an internal extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "Internal" number of the "Meeting Room" extension
	And I wait 2 seconds
	And the "Meeting Room" extension answers the call
	And I wait 1 second
	Then I should be connected to the "Internal" number of the "Meeting Room" extension

@Smoke
Scenario: Calling the DDI of an extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "External" number of the "Meeting Room" extension preceded by the outbound digit
	And I wait 3 seconds
	And the "Meeting Room" extension answers the call
	And I wait 1 second
	Then I should be connected to the "External" number of the "Meeting Room" extension

@Smoke
Scenario: Calling a local number with area code
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "01932566503" preceded by the outbound digit
	And I wait 6 seconds
	Then I should be connected to "01932566503"
	And I should be receiving audio

Scenario: Calling an international number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "0012123157494" preceded by the outbound digit
	And I wait 4 seconds
	Then I should be connected to "0012123157494"
	And I should be receiving audio

Scenario: Calling a 0800 number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "08000324829" preceded by the outbound digit
	And I wait 3 seconds
	Then I should be connected to "08000324829"
	And I should be receiving audio

Scenario: Calling a 0844 number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "08448650011" preceded by the outbound digit
	And I wait 3 seconds
	Then I should be connected to "08448650011"
	And I should be receiving audio

Scenario: Calling a 0845 number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "08457555555" preceded by the outbound digit
	And I wait 6 seconds
	Then I should be connected to "08457555555"
	And I should be receiving audio
	
Scenario: Calling a 0870 number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "08709039999" preceded by the outbound digit
	And I wait 3 seconds
	Then I should be connected to "08709039999"
	And I should be receiving audio

Scenario: Calling a 0871 number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "08712002233" preceded by the outbound digit
	And I wait 3 seconds
	Then I should be connected to "08712002233"
	And I should be receiving audio