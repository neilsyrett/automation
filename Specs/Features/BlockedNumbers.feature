﻿@BlockedNumbers
Feature: Blocked Numbers
	In order to avoid illicit activity
	As a CallStream administrator
	I want to be sure that users cannot call certain numbers

Scenario: Calling a number in Afghanistan
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "0093799654000" preceded by the outbound digit
	And I wait 1 second
	Then the call should be blocked
	And I should not be connected to "0093799654000"

Scenario: Calling a number in North Korea
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "0085023817982" preceded by the outbound digit
	And I wait 1 second
	Then the call should be blocked
	And I should not be connected to "0085023817982"

Scenario: Calling an international premium rate number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "0097912351" preceded by the outbound digit
	And I wait 1 second
	Then the call should be blocked
	And I should not be connected to "0097912351"