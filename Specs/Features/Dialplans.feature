﻿@Dialplans
Feature: Dialplans
	In order to prevent my incomming calls from being missed
	As a CallStream user
	I want my incomming calls to be redirected if not answered

Scenario: Calling into an extension configured to use the "Ring Internal, Ring External" dialplan - Internal leg
	Given I am connected to the CallStream network with the "Meeting Room" extension
	And another user is connected to the CallStream network with the "Office" extension
	And the "Office" extension dialplan is configured to first call "Internal" for 5 seconds
	And the "Office" extension dialplan is configured to then call "Mobile" for 35 seconds
	And the "Office" extension dialplan is configured to then call "Voicemail"
	When I dial the "External" number of the "Office" extension preceded by the outbound digit
	And I wait 2 seconds
	And the "Office" extension answers the call
	Then I should be connected to the "External" number of the "Office" extension
	And I should be receiving audio

Scenario: Calling into an extension configured to use the "Ring Internal, Ring External" dialplan - Voicemail leg
	Given I am connected to the CallStream network with the "Meeting Room" extension
	And another user is connected to the CallStream network with the "Office" extension
	And the "Office" extension dialplan is configured to first call "Internal" for 5 seconds
	And the "Office" extension dialplan is configured to then call "Mobile" for 35 seconds
	And the "Office" extension dialplan is configured to then call "Voicemail"
	When I dial the "External" number of the "Office" extension preceded by the outbound digit
	And I wait 50 seconds
	Then the call should be answered
	And I should be receiving audio

@Smoke
Scenario: Calling into an extension configured to use the "Ring Internal, Voicemail" dialplan - Internal leg
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	And the "Meeting Room" extension dialplan is configured to first call "Internal" for 5 seconds
	And the "Meeting Room" extension dialplan is configured to then call "Voicemail"
	When I dial the "External" number of the "Meeting Room" extension preceded by the outbound digit
	And I wait 2 seconds
	And the "Meeting Room" extension answers the call
	Then I should be connected to the "External" number of the "Meeting Room" extension
	And I should be receiving audio

@Smoke
Scenario: Calling into an extension configured to use the "Ring Internal, Voicemail" dialplan - Voicemail leg
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	And the "Meeting Room" extension dialplan is configured to first call "Internal" for 5 seconds
	And the "Meeting Room" extension dialplan is configured to then call "Voicemail"
	When I dial the "External" number of the "Meeting Room" extension preceded by the outbound digit
	And I wait 10 seconds
	Then the call should be answered
	And I should be receiving audio