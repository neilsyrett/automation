﻿@CallTransfer
Feature: CallTransfer
	In order to prevent a caller from having to redial
	As a CallStream user
	I want to be able to transfer a caller to another extension

Scenario: Attended transferring an incoming call to another extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Secretary" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "Internal" number of the "Meeting Room" extension
	And I wait 1 second
	And the "Secretary" extension answers the call
	And I wait 1 second
	And the "Secretary" extension dials the "Internal" number of the "Meeting Room" extension
	And I wait 1 second
	And the "Meeting Room" extension answers the call
	And I wait 1 second
	And the "Secretary" extension transfers the call to the "Internal" number of the "Meeting Room" extension
	Then I should be connected to the "Internal" number of the "Meeting Room" extension

Scenario: Blind transferring an incoming call to another extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Secretary" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "Internal" number of the "Meeting Room" extension
	And I wait 1 second
	And the "Secretary" extension answers the call
	And I wait 1 second
	And the "Secretary" extension blind transfers the call to the "Internal" number of the "Meeting Room" extension
	And I wait 1 second
	And the "Meeting Room" extension answers the call
	And I wait 1 second
	Then I should be connected to the "Internal" number of the "Meeting Room" extension