﻿@ManagerLogin @Web
Feature: ManagerLogin
	In order to manage CallStream
	As a member of centre staff
	I want to be able to login to the CallStream Manager website

@Smoke
Scenario: Successful CallStream Manager login
	Given I have navigated to the CallStream login page
	And I have entered the "username" of the "UK-EIRE CSR" user into the "username" field
	And I have entered the "password" of the "UK-EIRE CSR" user into the "password" field
	When I click the "login" button
	Then the "CallStream Manager" page will be displayed

@Smoke
Scenario: Unsuccessful CallStream Manager login attempt - blank password
	Given I have navigated to the CallStream login page
	And I have entered the "username" of the "UK-EIRE CSR" user into the "username" field
	And I have left the "username" field blank
	When I click the "login" button
	Then the "Unknown user or invalid password" error text will be displayed

@Smoke
Scenario: Unsuccessful CallStream Manager login attempt - random password
	Given I have navigated to the CallStream login page
	And I have entered the "username" of the "UK-EIRE CSR" user into the "username" field
	And I have entered the incorrect password of the "UK-EIRE CSR" user into the "password" field
	When I click the "login" button
	Then the "Unknown user or invalid password" error text will be displayed
