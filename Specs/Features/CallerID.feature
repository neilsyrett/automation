﻿@CallerID
Feature: CallerID
	In order to protect my identity
	As a CallStream user
	I want to be able to show or hide my number when making external calls

Scenario: Calling an external number with my Caller ID exposed
	Given I am connected to the CallStream network with the "Office" extension
	And the "Office" extension has Caller ID set to "Mine"
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "External" number of the "Meeting Room" extension preceded by the outbound digit
	And I wait 3 seconds
	And the "Meeting Room" extension answers the call
	Then the "Meeting Room" extension should have received a call from the Caller ID of the "Office" extension

Scenario: Calling an external number with my Caller ID hidden
	Given I am connected to the CallStream network with the "Meeting Room" extension
	And the "Meeting Room" extension has Caller ID set to "Hidden"
	And another user is connected to the CallStream network with the "Office" extension
	When I dial the "External" number of the "Office" extension preceded by the outbound digit
	And I wait 3 seconds
	And the "Office" extension answers the call
	Then the "Office" extension should have received a call from "Anonymous"