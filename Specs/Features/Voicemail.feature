﻿@Voicemail
Feature: Voicemail
	In order to retrieve my voicemail messages
	As a CallStream user
	I want to be able to dial the voicemail service and pick up my messages

Scenario: Calling Voicemail from internal
	Given I am connected to the CallStream network with the "Office" extension
	When I dial The "Voicemail extension"
	And I wait 1 second
	Then I should be connected to the "Voicemail extension"
	And I should be receiving audio

Scenario: Calling Voicemail from external
	Given I am connected to the CallStream network with the "Office" extension
	When I dial the "Voicemail DDI" preceded by the outbound digit
	And I wait 1 second
	Then I should be connected to the "Voicemail DDI"
	And I should be receiving audio

Scenario: Leaving a Voicemail message
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	And I know how many read and unread Voicemail messages the "Meeting Room" extension has
	When I dial the "External" number of the "Meeting Room" extension
	And nobody answers the call
	And I wait 60 seconds
	And I hang up
	And I wait 5 seconds
	Then the "Meeting Room" extension should have 1 more unread Voicemail message

Scenario: Retrieving a Voicemail message
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	And I know how many read and unread Voicemail messages the "Meeting Room" extension has
	When I dial the "External" number of the "Meeting Room" extension
	And nobody answers the call
	And I wait 50 seconds
	And I hang up
	And I wait 5 seconds
	And the "Meeting Room" extension dials "Voicemail"
	And waits 15 seconds
	And the "Meeting Room" extension keys "1"
	And waits 15 seconds
	And the "Meeting Room" extension hangs up
	Then the "Meeting Room" extension should have the same number of unread Voicemail messages
	And the "Meeting Room" extension should have 1 more read Voicemail messages