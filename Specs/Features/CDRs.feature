﻿@CDRs
Feature: CDRs
	In order to generate billing for telephone calls
	As an Account Manager
	I want to be able to obtain call records for phone calls made and received

@Smoke
Scenario: Generate a CDR for a call to an internal extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "Internal" number of the "Meeting Room" extension
	And I wait 2 seconds
	And the "Meeting Room" extension answers the call
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 1 CDR should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be the "Internal" number of the "Meeting Room" extension
	And the Extension on the "first" CDR should be the "Internal" number of the "Meeting Room" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "Internal"
	And the Call Time on the "first" CDR should be 12 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"

Scenario: Generate a CDR for a call to the DDI of an internal extension
	Given I am connected to the CallStream network with the "Office" extension
	And another user is connected to the CallStream network with the "Meeting Room" extension
	When I dial the "External" number of the "Meeting Room" extension
	And I wait 2 seconds
	And the "Meeting Room" extension answers the call
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 2 CDRs should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be the "External" number of the "Meeting Room" extension
	And the Extension on the "first" CDR should be the "Internal" number of the "Office" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "OutboundCall"
	And the Call Time on the "first" CDR should be 12 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"
	And the From number on the "second" CDR should be the "External" number of the "Office" extension
	And the To number on the "second" CDR should be the "Internal" number of the "Meeting Room" extension
	And the Extension on the "second" CDR should be the "Internal" number of the "Meeting Room" extension
	And the Account Code on the "second" CDR should be the account code of the "Office" extension
	And the Call Type on the "second" CDR should be "InboundCall"
	And the Call Time on the "second" CDR should be 12 seconds
	And the Bill Time on the "second" CDR should be 10 seconds
	And the Country Code on the "second" CDR should be "GB"
	And the Export Status on the "second" CDR should be "New"

Scenario: Generate a CDR for a call to an external number
	Given I am connected to the CallStream network with the "Office" extension
	When I dial "01932566503" preceded by the outbound digit
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 1 CDR should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be "01932566503"
	And the Extension on the "first" CDR should be the "Internal" number of the "Office" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "OutboundCall"
	And the Call Time on the "first" CDR should be 10 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"

Scenario: Generate a CDR for a call to the Reception extension
	Given I am connected to the CallStream network with the "Office" extension
	When I dial The "Voicemail extension"
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 1 CDR should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be the "Reception Extension"
	And the Extension on the "first" CDR should be the "Internal" number of the "Office" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "InboundCallToReception"
	And the Call Time on the "first" CDR should be 10 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"

Scenario: Generate a CDR for a call to the Voicemail extension
	Given I am connected to the CallStream network with the "Office" extension
	When I dial The "Voicemail extension"
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 1 CDR should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be the "Voicemail Extension"
	And the Extension on the "first" CDR should be the "Internal" number of the "Office" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "VoicemailTransfer"
	And the Call Time on the "first" CDR should be 10 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"

Scenario: Generate a CDR for a call to the Voicemail DDI
	Given I am connected to the CallStream network with the "Office" extension
	When I dial the "Voicemail DDI" preceded by the outbound digit
	And I wait 10 seconds
	And I hang up
	And I wait 60 seconds
	Then 2 CDRs should be created for the call
	And the From number on the "first" CDR should be the "Internal" number of the "Office" extension
	And the To number on the "first" CDR should be the "Voicemail DDI"
	And the Extension on the "first" CDR should be the "Internal" number of the "Office" extension
	And the Account Code on the "first" CDR should be the account code of the "Office" extension
	And the Call Type on the "first" CDR should be "OutboundCall"
	And the Call Time on the "first" CDR should be 10 seconds
	And the Bill Time on the "first" CDR should be 10 seconds
	And the Country Code on the "first" CDR should be "GB"
	And the Export Status on the "first" CDR should be "New"
	And the From number on the "second" CDR should be the "External" number of the "Office" extension
	And the To number on the "second" CDR should be the "Voicemail Extension"
	And the Extension on the "second" CDR should be the "Voicemail Extension"
	And the Account Code on the "second" CDR should be the account code of the "Office" extension
	And the Call Type on the "second" CDR should be "InboundCall"
	And the Call Time on the "second" CDR should be 10 seconds
	And the Bill Time on the "second" CDR should be 10 seconds
	And the Country Code on the "second" CDR should be "GB"
	And the Export Status on the "second" CDR should be "New"