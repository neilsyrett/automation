﻿@HandsetConfiguration
Feature: Handset Configuration
	In order to configure a new handset
	As a CallStream user
	I want to be able to connect a new handset to the system and have the settings delivered to the handset

Scenario: Connecting a new Cisco SPA508G
	Given I have a "Cisco" "SPA508G" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Cisco SPA508G
	Given I have a "Cisco" "SPA508G" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Cisco SPA525G
	Given I have a "Cisco" "SPA525G" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Cisco SPA525G
	Given I have a "Cisco" "SPA525G" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

@Smoke
Scenario: Connecting a new Cisco SPA525G2
	Given I have a "Cisco" "SPA525G2" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset

@Smoke
Scenario: Provisioning a new Cisco SPA525G2
	Given I have a "Cisco" "SPA525G2" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Polycom IP330
	Given I have a "Polycom" "IP330" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Polycom IP330
	Given I have a "Polycom" "IP330" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

@Smoke
Scenario: Connecting a new Polycom IP331
	Given I have a "Polycom" "IP331" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset

@Smoke	
Scenario: Provisioning a new Polycom IP331
	Given I have a "Polycom" "IP331" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Polycom IP450
	Given I have a "Polycom" "IP450" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Polycom IP450
	Given I have a "Polycom" "IP450" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Polycom IP5000
	Given I have a "Polycom" "IP5000" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Polycom IP5000
	Given I have a "Polycom" "IP5000" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Polycom VVX500
	Given I have a "Polycom" "VVX500" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Polycom VVX500
	Given I have a "Polycom" "VVX500" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

Scenario: Connecting a new Polycom Duo
	Given I have a "Polycom" "Duo" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset
	
Scenario: Provisioning a new Polycom Duo
	Given I have a "Polycom" "Duo" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset

@Smoke
Scenario: Connecting a new Snom 370
	Given I have a "Snom" "370" handset
	And the MAC Address of the handset is unknown to CallStream
	When I connect the handset to the CallStream network
	Then the initial configuration settings are delivered to the handset

@Smoke
Scenario: Provisioning a new Snom 370
	Given I have a "Snom" "370" handset
	And the MAC Address of the handset is unknown to CallStream
	And I am connected to the CallStream network with the Provisioning extension
	When I dial the provisioning number
	And I wait 2 seconds
	And I key the provisioning password
	And I key "#"
	And I wait 9 seconds
	And I key the "Internal" number of the "Office" extension
	And I wait 13 seconds
	And I hang up
	And I connect the handset to the CallStream network
	Then the full configuration settings for the "Office" extension are delivered to the handset