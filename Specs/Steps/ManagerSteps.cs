﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using Regus.CallStreamSmokeSuite.Environment;
using Regus.CallStreamSmokeSuite.Helpers;
using System.Threading;
using System.Text.RegularExpressions;
using System.Drawing;

namespace Regus.CallStreamSmokeSuite.Steps
{
    [Binding]
    public class ManagerSteps : BeforeAfter
    {
        [Given(@"(?i)I have navigated to the CallStream login page")]
        public void GivenIHaveNavigatedToTheCallStreamLoginPage()
        {
            currentPage = new LoginPage();
            currentPage.NavigateTo(webDriver);
        }

        [Given(@"(?i)I have entered the ""(Username|Password)"" of the ""(.*)"" user into the ""(.*)"" field")]
        public void GivenIHaveEnteredTheOfTheUserIntoTheField(string propertyName, string alias, string fieldName)
        {
            string userInput = EnvironmentApi.Settings.CallStream.GetUser(alias).GetProperty(propertyName);
            ((IUserInputPage)currentPage).EnterValueIntoInputField(fieldName, userInput);
        }

        [Given(@"I have entered the incorrect password of the ""(.*)"" user into the ""(.*)"" field")]
        public void GivenIHaveEnteredTheIncorrectPasswordOfTheUserIntoTheField(string alias, string fieldName)
        {
            string randomPassword = String.Empty;
            randomPassword = randomPassword.Random(100);
            string userPassword = EnvironmentApi.Settings.CallStream.GetUser(alias).Password;

            //check that randomPassword isn't the same as the user's actual password
            while (randomPassword == userPassword)
            {
                randomPassword = randomPassword.Random();
            }

            ((IUserInputPage)currentPage).EnterValueIntoInputField(fieldName, randomPassword);
        }

        [Given(@"I have left the ""(.*)"" field blank")]
        public void GivenIHaveLeftTheFieldBlank(string fieldName)
        {
            ((IUserInputPage)currentPage).ClearUserInputField(fieldName);
        }

        [When(@"(?i)I click the ""(.*)"" button")]
        public void WhenIClickTheButton(string buttonName)
        {
            ((IUserInputPage)currentPage).ClickButton(buttonName);
        }

        [Then(@"(?i)the ""(.*)"" page will be displayed")]
        public void ThenThePageWillBeDisplayed(string pageTitle)
        {
            // Warning: waitForTextPresent may require manual changes
            for (int second = 0; ; second++)
            {
                if (second >= 100) Assert.Fail("timeout");
                try
                {
                    if (Regex.IsMatch(currentPage.Driver.FindElement(By.CssSelector("BODY")).Text, "^[\\s\\S]*$")) break;

                    //IWebElement e = driver.FindElement(By.XPath("//ul[@id='mainmenu']/li[0]"));
                }
                catch (Exception)
                { }
                Thread.Sleep(100);
            }
            Assert.AreEqual(pageTitle, currentPage.Title);
        }

        [Then(@"the ""(.*)"" error text will be displayed")]
        public void ThenTheErrorTextWillBeDisplayed(string errorText)
        {
            Assert.IsTrue(currentPage.ErrorText.Contains(errorText));
        }
    }
}
