﻿using System;
using System.Diagnostics;
using TechTalk.SpecFlow;
using NUnit.Framework;
using Regus.SipTester;
using System.Threading;
using Regus.CallStreamSmokeSuite.Environment;
using Regus.CallStreamSmokeSuite.Helpers;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.IO;

namespace Regus.CallStreamSmokeSuite.Steps
{
    [Binding]
    public class CallsSteps : BeforeAfter
    {
        Call incommingCall;
        Collection<CallRecord> callRecords;
        SipAccount callingExtension;
        FullExtension calledExtension;
        double voicemailMessagesUnread;
        double voicemailMessagesRead;
        int dialplanLeg;
        string handsetMake;
        string handsetModel;
        string response;
        bool outboundDigit;

        [Given(@"(?i)I am connected to the CallStream network with the ""(.*)"" extension")]
        public void GivenIAmConnectedToTheCallStreamNetworkWithTheExtension(string extensionName)
        {
            callingExtension = (SipAccount)EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.IsTrue(callingExtension.Connect());
        }

        [Given(@"(?i)I am connected to the CallStream network with the Provisioning extension")]
        public void GivenIAmConnectedToTheCallStreamNetworkWithTheProvisioningExtension()
        {
            callingExtension = EnvironmentApi.Settings.CallStream.ProvisionExtension;
            Assert.IsTrue(callingExtension.Connect());
        }

        [Given(@"(?i)another user is connected to the CallStream network with the ""(.*)"" extension")]
        public void GivenAnotherUserIsConnectedToTheCallStreamNetworkWithTheExtension(string extensionName)
        {
            calledExtension = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.IsTrue(calledExtension.Connect());
        }

        [Given(@"(?i)the ""(.*)"" extension is configured to use the ""(.*)"" dialplan")]
        public void GivenTheExtensionIsConfiguredToUseTheDialplan(string extensionName, string dialplan)
        {
            Assert.AreEqual(dialplan, EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.Name);
        }

        [Given(@"(?i)the ""(.*)"" extension dialplan is configured to first call ""(Internal|External|Mobile|Home|Emergency|Other|Voicemail|Reception)"" for (\d*) seconds")]
        public void GivenTheExtensionDialplanIsConfiguredToFirstCallForSeconds(string extensionName, string number, int ringForSeconds)
        {
            dialplanLeg = 0;
            Assert.AreEqual(number.NameToNumber(), EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.DialplanLegs[dialplanLeg].Number);
            Assert.AreEqual(ringForSeconds, EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.DialplanLegs[dialplanLeg].RingForSeconds);
        }

        [Given(@"(?i)the ""(.*)"" extension dialplan is configured to then call ""(Internal|External|Mobile|Home|Emergency|Other|Voicemail|Reception)"" for (\d*) seconds")]
        public void GivenTheExtensionDialplanIsConfiguredToThenCallForSeconds(string extensionName, string number, int ringForSeconds)
        {
            dialplanLeg++;
            Assert.AreEqual(number.NameToNumber(), EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.DialplanLegs[dialplanLeg].Number);
            Assert.AreEqual(ringForSeconds, EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.DialplanLegs[dialplanLeg].RingForSeconds);
        }

        [Given(@"(?i)the ""(.*)"" extension dialplan is configured to then call ""(Internal|External|Mobile|Home|Emergency|Other|Voicemail|Reception)""")]
        public void GivenTheExtensionDialplanIsConfiguredToThenCall(string extensionName, string number)
        {
            dialplanLeg++;
            Assert.AreEqual(number.NameToNumber(), EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dialplan.DialplanLegs[dialplanLeg].Number);
        }

        [Given(@"(?i)I know how many read and unread Voicemail messages I have")]
        public void GivenIKnowHowManyReadAndUnreadVoicemailMessagesIHave()
        {
            voicemailMessagesUnread = callingExtension.VoicemailMessagesUnread;
        }

        [Given(@"(?i)I know how many read and unread Voicemail messages the ""(.*)"" extension has")]
        public void GivenIKnowHowManyReadAndUnreadVoicemailMessagesTheExtensionHas(string extensionName)
        {
            SipAccount calledExtension = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            voicemailMessagesRead = calledExtension.VoicemailMessagesRead;
            voicemailMessagesUnread = calledExtension.VoicemailMessagesUnread;
        }

        [Given(@"(?i)the ""(.*)"" extension has Caller ID set to ""(Hidden|Mine)""")]
        public void GivenTheExtensionHasCallerIDSetTo(string extensionName, string callerID)
        {
            Assert.AreEqual(callerID, EnvironmentApi.Settings.CallStream.GetExtension(extensionName).OutboundCallerID);
        }

        [Given(@"(?i)I have a ""(.*)"" ""(.*)"" handset")]
        public void GivenIHaveAHandset(string make, string model)
        {
            handsetMake = make;
            handsetModel = model;
        }

        [Given(@"(?i)the MAC Address of the handset is unknown to CallStream")]
        public void GivenTheMACAddressOfTheHandsetIsUnknownToCallStream()
        {
            SshApi.ClearMacAddress();
            DatabaseApi.UnassignHandsets();
        }

        [When(@"(?i)I dial the ""(Internal|External)"" number of the ""(.*)"" extension")]
        public void WhenIDialTheInternalNumberOfTheExtension(string number, string extensionName)
        {
            callingExtension.Dial(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number));
        }

        [When(@"(?i)I dial the ""(Internal|External)"" number of the ""(.*)"" extension preceded by the outbound digit")]
        public void WhenIDialTheInternalNumberOfTheExtensionPrecededByTheOutboundDigit(string number, string extensionName)
        {
            outboundDigit = true;
            callingExtension.Dial(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number), outboundDigit);
        }

        [When(@"(?i)I dial the ""(Reception extension|Voicemail extension)""")]
        public void WhenIDialThe(string number)
        {
            callingExtension.Dial(EnvironmentApi.Settings.CallStream.GetNumber(number));
        }

        [When(@"(?i)I dial the provisioning number")]
        public void WhenIDialTheProvisioningNumber()
        {
            callingExtension.Dial(EnvironmentApi.Settings.CallStream.ProvisionNumber);
        }

        [When(@"(?i)I dial ""([0-9*#]*)""")]
        public void WhenIDial(string number)
        {
            callingExtension.Dial(number);
        }

        [When(@"(?i)I dial ""([0-9*#]*)"" preceded by the outbound digit")]
        public void WhenIDialPrecededByTheOutboundDigit(string number)
        {
            outboundDigit = true;
            callingExtension.Dial(number, true);
        }

        [When(@"(?i)I dial the ""(Reception Extension|Voicemail Extension|Voicemail DDI)"" preceded by the outbound digit")]
        public void WhenIDialThePrecededByTheOutboundDigit(string number)
        {
            outboundDigit = true;
            callingExtension.Dial(EnvironmentApi.Settings.CallStream.GetNumber(number), true);
        }

        [When(@"(?i)I? ?waits? (\d+) seconds?")]
        public void WhenIWaitSeconds(int delaySeconds)
        {
            Thread.Sleep(delaySeconds * 1000);
        }

        [When(@"(?i)I hang up")]
        public void WhenIHangUp()
        {
            callingExtension.Hangup();
        }

        [When(@"(?i)the ""(.*)"" extension hangs up")]
        public void WhenTheExtensionHangsUp(string extensionName)
        {
            EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Hangup();
        }

        [When(@"(?i)the ""(.*)"" extension answers the call")]
        public void WhenTheExtensionAnswersTheCall(string extensionName)
        {
            incommingCall = EnvironmentApi.Settings.CallStream.GetExtension(extensionName).AnswerNextCall();
        }

        [When(@"(?i)nobody answers the call")]
        public void WhenNobodyAnswersTheCall()
        {
            //Do nothing
        }

        [When(@"(?i)the ""(.*)"" extension dials ""(.*)""")]
        public void WhenTheExtensionDials(string extensionName, string number)
        {
            EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Dial(number);
        }

        [When(@"(?i)I key ""([0-9*#]*)""")]
        public void WhenIKey(string keys)
        {
            callingExtension.SendTones(keys);
        }

        [When(@"(?i)the ""(.*)"" extension keys ""([0-9*#]*)""")]
        public void WhenTheExtensionKeys(string extensionName, string keys)
        {
            EnvironmentApi.Settings.CallStream.GetExtension(extensionName).SendTones(keys);
        }

        [When(@"(?i)I connect the handset to the CallStream network")]
        public void WhenIConnectTheHandsetToTheCallStreamNetwork()
        {
            response = HandsetApi.SendConfigurationRequest(handsetMake, handsetModel);
        }

        [When(@"(?i)I key the provisioning password")]
        public void WhenIKeyTheProvisioningPassword()
        {
            callingExtension.SendTones(EnvironmentApi.Settings.CallStream.ProvisionPassword);
        }

        [When(@"(?i)I key the ""(Internal|External)"" number of the ""(.*)"" extension")]
        public void WhenIKeyTheNumberOfTheExtension(string number, string extensionName)
        {
            callingExtension.SendTones(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number));
        }

        [When(@"(?i)the ""(.*)"" extension dials the ""(Internal|External)"" number of the ""(.*)"" extension")]
        public void WhenTheExtensionDialsTheNumberOfTheExtension(string fromExtension, string number, string toExtension)
        {
            EnvironmentApi.Settings.CallStream.GetExtension(fromExtension).Dial(EnvironmentApi.Settings.CallStream.GetExtension(toExtension), number);
        }

        [When(@"(?i)the ""(.*)"" extension blind transfers the call to the ""(Internal|External)"" number of the ""(.*)"" extension")]
        public void WhenTheExtensionBlindTransfersTheCallToTheNumberOfTheExtension(string fromExtension, string number, string toExtension)
        {
            //EnvironmentApi.Settings.CallStream.GetExtension(fromExtension)
        }

        [When(@"(?i)the ""(.*)"" extension transfers the call to the ""(Internal|External)"" number of the ""(.*)"" extension")]
        public void WhenTheExtensionTransfersTheCallToTheNumberOfTheExtension(string fromExtension, string number, string toExtension)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"(?i)I should be connected to the ""(Internal|External|Mobile|Home|Emergency|Other)"" number of the ""(.*)"" extension")]
        public void ThenIShouldBeConnectedToTheNumberOfTheExtension(string number, string extensionName)
        {
            string expectedNumber = EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number);
            if (outboundDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                expectedNumber = EnvironmentApi.Settings.CallStream.OutboundDigit + expectedNumber;

            Assert.AreEqual(expectedNumber, callingExtension.GetRemoteNumber());
        }

        [Then(@"(?i)I should be connected to the ""(Reception Extension|Voicemail Extension|Voicemail DDI)""")]
        public void ThenIShouldBeConnectedToThe(string extensionName)
        {
            string expectedNumber = EnvironmentApi.Settings.CallStream.GetNumber(extensionName);
            if (outboundDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                expectedNumber = EnvironmentApi.Settings.CallStream.OutboundDigit + expectedNumber;

            Assert.AreEqual(expectedNumber, callingExtension.GetRemoteNumber());
        }

        [Then(@"(?i)I should be connected to ""([0-9*#]*)""")]
        public void ThenIShouldBeConnectedTo(string expectedNumber)
        {
            if (outboundDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                expectedNumber = EnvironmentApi.Settings.CallStream.OutboundDigit + expectedNumber;

            Assert.AreEqual(expectedNumber, callingExtension.GetRemoteNumber());
        }

        [Then(@"(?i)I should not be connected to ""([0-9*#]*)""")]
        public void ThenIShouldNotBeConnectedTo(string expectedNumber)
        {
            if (outboundDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                expectedNumber = EnvironmentApi.Settings.CallStream.OutboundDigit + expectedNumber;

            Assert.AreNotEqual(expectedNumber, callingExtension.GetRemoteNumber());
        }

        [Then(@"(?i)I should be receiving audio")]
        public void ThenIShouldBeReceivingAudio()
        {
            Assert.Greater(callingExtension.LastCall.RtpEngine.BytesReceived, 0);
            Thread.Sleep(5000);
        }

        [Then(@"(?i)(\d+) CDRs? should be created for the call")]
        public void ThenCDRsShouldBeCreatedForTheCall(int cdrCount)
        {
            callRecords = CdrApi.GetCallRecords(callingExtension.LastCall);
            Assert.AreEqual(cdrCount, callRecords.Count);
        }

        [Then(@"(?i)the Extension on the ""(.*)"" CDR should be the ""(Internal|External|Mobile|Home|Emergency|Other)"" number of the ""(.*)"" extension")]
        public void ThenTheExtensionOnTheCDRShouldBeTheNumberOfTheExtension(string recordNumber, string number, string extensionName)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number), callRecords[recordNumber.PositionToIndex()].Extension);
        }

        [Then(@"(?i)the Extension on the ""(.*)"" CDR should be the ""(Reception Extension|Voicemail Extension|Voicemail DDI)""")]
        public void ThenTheExtensionOnTheCDRShouldBeThe(string recordNumber, string number)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetNumber(number), callRecords[recordNumber.PositionToIndex()].Extension);
        }

        [Then(@"the Account Code on the ""(.*)"" CDR should be the account code of the ""(.*)"" extension")]
        public void ThenTheAccountCodeOnTheCDRShouldBeTheAccountCodeOfTheExtension(string recordNumber, string extensionName)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).Associate.AccountCode, callRecords[recordNumber.PositionToIndex()].AccountCode);
        }

        [Then(@"(?i)the Extension on the ""(.*)"" CDR should be ""(\d+)""")]
        public void ThenTheExtensionOnTheCDRShouldBe(string recordNumber, string number)
        {
            Assert.AreEqual(number, callRecords[recordNumber.PositionToIndex()].Extension);
        }

        [Then(@"(?i)the From number on the ""(.*)"" CDR should be the ""(Internal|External|Mobile|Home|Emergency|Other)"" number of the ""(.*)"" extension")]
        public void ThenTheFromNumberOnTheCDRShouldBeTheNumberOfTheExtension(string recordNumber, string number, string extensionName)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number), callRecords[recordNumber.PositionToIndex()].From);
        }

        [Then(@"(?i)the To number on the ""(.*)"" CDR should be the ""(Internal|External|Mobile|Home|Emergency|Other)"" number of the ""(.*)"" extension")]
        public void ThenTheToNumberOnTheCDRShouldBeTheNumberOfTheExtension(string recordNumber, string number, string extensionName)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetExtension(extensionName).GetNumber(number), callRecords[recordNumber.PositionToIndex()].To);
        }

        [Then(@"(?i)the To number on the ""(.*)"" CDR should be the ""(Reception Extension|Voicemail Extension|Voicemail DDI)""")]
        public void ThenTheToNumberOnTheCDRShouldBeThe(string recordNumber, string number)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetNumber(number), callRecords[recordNumber.PositionToIndex()].To);
        }

        [Then(@"(?i)the To number on the ""(.*)"" CDR should be ""(\d+)""")]
        public void ThenTheToNumberOnTheCDRShouldBe(string recordNumber, string number)
        {
            Assert.AreEqual(number, callRecords[recordNumber.PositionToIndex()].To);
        }

        [Then(@"(?i)the Call Type on the ""(.*)"" CDR should be ""(InboundCall|InboundCallToReception|InboundFax|Internal|OutboundCall|OutboundTransfer|VoicemailTransfer)""")]
        public void ThenTheCallTypeOnTheCDRShouldBe(string recordNumber, string callType)
        {
            Assert.AreEqual(callType, callRecords[recordNumber.PositionToIndex()].CallType);
        }

        [Then(@"(?i)the Call Time on the ""(.*)"" CDR should be (\d+) seconds?")]
        public void ThenTheCallTimeOnTheCDRShouldBeSeconds(string recordNumber, double callTime)
        {
            Assert.AreEqual(callTime, callRecords[recordNumber.PositionToIndex()].CallTime, EnvironmentApi.Settings.CDRToleranceSeconds);
        }

        [Then(@"(?i)the Bill Time on the ""(.*)"" CDR should be (\d+) seconds?")]
        public void ThenTheBillTimeOnTheCDRShouldBeSeconds(string recordNumber, double billTime)
        {
            Assert.AreEqual(billTime, callRecords[recordNumber.PositionToIndex()].BillTime, EnvironmentApi.Settings.CDRToleranceSeconds);
        }

        [Then(@"(?i)the Country Code on the ""(.*)"" CDR should be ""(.*)""")]
        public void ThenTheCountryCodeOnTheCDRShouldBeSeconds(string recordNumber, string countryCode)
        {
            Assert.AreEqual(countryCode, callRecords[recordNumber.PositionToIndex()].CountryCode);
        }

        [Then(@"(?i)the Export Status on the ""(.*)"" CDR should be ""(.*)""")]
        public void ThenTheExportStatusOnTheCDRShouldBeSeconds(string recordNumber, string exportStatus)
        {
            Assert.AreEqual(exportStatus, callRecords[recordNumber.PositionToIndex()].ExportStatus);
        }

        [Then(@"(?i)I should have (\d*) more unread Voicemail message")]
        public void ThenIShouldHaveMoreUnreadVoicemailMessage(int messagesLeft)
        {
            Assert.AreEqual(voicemailMessagesUnread + messagesLeft, callingExtension.VoicemailMessagesUnread);
        }

        [Then(@"(?i)the ""(.*)"" extension should have (\d*) more unread Voicemail message")]
        public void ThenTheExtensionShouldHaveMoreUnreadVoicemailMessage(string extensionName, int messagesLeft)
        {
            SipAccount extensionCalled = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.AreEqual(voicemailMessagesUnread + messagesLeft, extensionCalled.VoicemailMessagesUnread);
        }

        [Then(@"(?i)the ""(.*)"" extension should have (\d*) less unread Voicemail message")]
        public void ThenTheExtensionShouldHaveLessUnreadVoicemailMessage(string extensionName, int messagesRead)
        {
            SipAccount extensionCalled = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.AreEqual(voicemailMessagesUnread - messagesRead, extensionCalled.VoicemailMessagesUnread);
        }

        [Then(@"(?i)the ""(.*)"" extension should have the same number of unread Voicemail messages?")]
        public void ThenTheExtensionShouldHaveTheSameNmberOfUnreadVoicemailMessages(string extensionName)
        {
            SipAccount extensionCalled = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.AreEqual(voicemailMessagesUnread, extensionCalled.VoicemailMessagesUnread);
        }

        [Then(@"(?i)the ""(.*)"" extension should have (.*) more read Voicemail messages?")]
        public void ThenTheExtensionShouldHaveMoreReadVoicemailMessages(string extensionName, int messagesRead)
        {
            SipAccount extensionCalled = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            Assert.AreEqual(voicemailMessagesRead + messagesRead, extensionCalled.VoicemailMessagesRead);
        }

        [Then(@"(?i)the ""(.*)"" extension should have received a call from the Caller ID of the ""(.*)"" extension")]
        public void ThenTheExtensionShouldHaveReceivedACallFromTheCallerIDOfTheExtension(string calledExtensionName, string callerExtensionName)
        {
            Assert.AreEqual(EnvironmentApi.Settings.CallStream.GetExtension(callerExtensionName).OutboundCallerDisplayNumber, incommingCall.RequestResponse.Request.From.Name);
        }

        [Then(@"(?i)the ""(.*)"" extension should have received a call from ""(.*)""")]
        public void ThenTheExtensionShouldHaveReceivedACallFromAnAnonymousUser(string calledExtensionName, string callerID)
        {
            Assert.AreEqual(callerID, incommingCall.RequestResponse.Request.From.Name);
        }

        [Then(@"(?i)the call should be answered")]
        public void ThenTheCallShouldBeAnswered()
        {
            Assert.IsTrue(callingExtension.LastCall.IsConnected);
        }

        [Then(@"(?i)the initial configuration settings are delivered to the handset")]
        public void ThenTheInitialConfigurationSettingsAreDeliveredToTheHandset()
        {
            string templateConfig = HandsetApi.GetInitialConfiguration(handsetMake, handsetModel);
            Assert.IsTrue(HandsetApi.CompareInitialConfigurationFiles(handsetMake, handsetModel, templateConfig, response));
        }

        [Then(@"(?i)the full configuration settings for the ""(.*)"" extension are delivered to the handset")]
        public void ThenTheFullConfigurationSettingsForTheExtensionAreDeliveredToTheHandset(string extensionName)
        {
            FullExtension extension = EnvironmentApi.Settings.CallStream.GetExtension(extensionName);
            string templateConfig = HandsetApi.GetFullConfiguration(handsetMake, handsetModel, extension);
            Assert.IsTrue(HandsetApi.CompareFullConfigurationFiles(handsetMake, handsetModel, templateConfig, response));
        }

        [Then(@"(?i)the call should be blocked")]
        public void ThenTheCallShouldBeBlocked()
        {
            Assert.IsFalse(callingExtension.LastCall.IsConnected);
            Assert.AreEqual(603, callingExtension.LastCall.RequestResponse.Response.StatusCode);
        }
    }
}