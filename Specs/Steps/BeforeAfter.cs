﻿using BoDi;
using TechTalk.SpecFlow;
using Regus.CallStreamSmokeSuite.Helpers;
using System.Threading;
using Regus.CallStreamSmokeSuite.CommWizardService;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace Regus.CallStreamSmokeSuite.Environment
{
    [Binding]
    public class BeforeAfter
    {
        protected static IPage currentPage;
        protected static IWebDriver webDriver;

        [BeforeTestRun]
        public static void InitializeEnvironment()
        {
            //Initialise environment settings
            EnvironmentApi.InitialiseEnvironment();

            //Import settings
            #if DEBUG
            {
                EnvironmentApi.Settings = new HardcodedSettings();
            }
            #else
            {
                EnvironmentApi.Settings = new ConfigurableSettings();
            }
            #endif

            //Generate Fixtures if required
            if (EnvironmentApi.Settings.DatabaseFixturesCreate)
                DatabaseApi.CreateFixtures();
            else
                DatabaseApi.FindFixtures();
        }

        [BeforeScenario("Web")]
        public static void BeforeScenarioWeb()
        {
            webDriver = new InternetExplorerDriver();
            webDriver.Manage().Window.Maximize();
        }

        [AfterScenario("Web")]
        public static void AfterScenarioWeb()
        {
            try
            {
                currentPage.Driver.Quit();
            }
            catch { }
        }

        [AfterScenario]
        public static void AfterScenario()
        {
            //Hangup all calls after each scenario
            EnvironmentApi.Settings.CallStream.HangupOutgoing();
            Thread.Sleep(1000);
        }

        [AfterTestRun]
        public static void ClearDownEnvironment()
        {
            //driver.Quit();
            //Disconnect clients after test run
            //SipApi.Disconnect();

            //Delete Fixtures if requried
            if (EnvironmentApi.Settings.DatabaseFixturesDelete)
                DatabaseApi.DeleteFixtures();
        }
    }
}