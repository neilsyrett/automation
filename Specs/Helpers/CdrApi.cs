﻿using System;
using System.Diagnostics;
using TechTalk.SpecFlow;
using NUnit.Framework;
using System.Threading;
using Regus.SipTester;
using System.Collections.ObjectModel;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class CdrApi
    {
        public static Collection<CallRecord> GetCallRecords(Call call)
        {
            CallRecords.Server = EnvironmentApi.Settings.DatabaseIPAddress;
            CallRecords.Database = EnvironmentApi.Settings.DatabaseName;
            CallRecords.Username = EnvironmentApi.Settings.DatabaseUsername;
            CallRecords.Password = EnvironmentApi.Settings.DatabasePassword;
            return CallRecords.GetCallRecords(call.CallID);
        }
    }
}