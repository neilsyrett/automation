﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Regus.SipTester;
using System.Net.NetworkInformation;
using System.Threading;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class SshApi
    {
        public static void ClearMacAddress()
        {
            SshEngine sshEngine = new SshEngine();
            sshEngine.Connect(EnvironmentApi.Settings.CallStream.AsteriskIPAddress, EnvironmentApi.Settings.CallStream.AsteriskUsername, EnvironmentApi.Settings.CallStream.AsteriskPassword);
            Thread.Sleep(100);
            sshEngine.Transfer("asterisk -r");
            Thread.Sleep(100);
            sshEngine.Transfer("database deltree mac" + NetworkEngine.GetMacAddress(EnvironmentApi.Settings.NetworkAdaptorID).ToString().ToUpper());

            foreach (SipAccount extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                Thread.Sleep(100);
                sshEngine.Transfer("database deltree ext" + extension.Username + "/macaddress");
            }
            
            sshEngine.Transfer("exit");
            sshEngine.Disconnect();
        }
    }
}
