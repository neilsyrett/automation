﻿using System;
using Regus.SipTester;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class ProvisionExtension : SipAccount
    {
        public ProvisionExtension(string username, string secret)
            : base(username, secret)
        { }
    }
}