﻿using MySql.Data.MySqlClient;
using Regus.CallStreamSmokeSuite.Environment;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using Regus.CallStreamSmokeSuite.CommWizardService;
using System.Threading;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    internal static class DatabaseApi
    {
        private static MySqlConnection connection;
        private static OzCommWizardServicesPortTypeClient webClient;

        internal static void FindFixtures()
        {
            //Update Customer ID fields
            foreach (Customer customer in EnvironmentApi.Settings.CallStream.Customers)
            {
                UpdateCustomerID(customer);
            }

            //Update Associate ID fields
            foreach (Associate associate in EnvironmentApi.Settings.CallStream.Associates)
            {
                UpdateAssociateID(associate);
            }

            //Update Customer ID fields
            foreach (FullExtension extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                UpdateExtensionID(extension);
            }
            Disconnect();
        }

        internal static void CreateFixtures()
        {
            //Delete any existing fixtres first, but don't worry about failures
            DeleteFixtures(false);
            //Thread.Sleep(1000);

            //Create Customers
            foreach (Customer customer in EnvironmentApi.Settings.CallStream.Customers)
            {
                CreateCustomer(customer);
            }

            //Create Associates
            foreach (Associate associate in EnvironmentApi.Settings.CallStream.Associates)
            {
                CreateAssociate(associate);
            }

            //Create Extensions and assign to Associate
            foreach (FullExtension extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                CreateExtension(extension);
                AssignExtension(extension);
            }
            Disconnect();
        }

        internal static void UnassignHandsets()
        {
            //Create Extensions and assign to Associate
            foreach (FullExtension extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                ClearExtensionHandset(extension);
            }
            Disconnect();
        }

        internal static void DeleteFixtures(bool disconnect = true)
        {
            //Delete Extensions and assignments
            foreach (FullExtension extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                DeleteAssignment(extension);
                DeleteExtension(extension);
            }

            //Delete Associates
            foreach (Associate associate in EnvironmentApi.Settings.CallStream.Associates)
            {
                DeleteAssociate(associate);
            }

            //Delete Customers
            foreach (Customer customer in EnvironmentApi.Settings.CallStream.Customers)
            {
                DeleteCustomer(customer);
            }

            Disconnect(disconnect);
        }

        internal static CallStream GetCallStream(string centreNumber, string mainNumber, int inboundCarrierDigits, string asteriskUsername, string asteriskPassword, string provisionUsername, string provisionSecret, bool disconnect = false)
        {
            int locationID = GetLocationID(centreNumber);
            int commWizardID = GetCommWizardID(locationID);

            //Get CallStream details
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT * FROM CommWizards WHERE CommWizardID = {0} ORDER BY CommWizardID ASC LIMIT 1;", commWizardID);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            CallStream callStream;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                callStream = new CallStream(centreNumber, mainNumber, inboundCarrierDigits, asteriskUsername, asteriskPassword, provisionUsername, provisionSecret, locationID, commWizardID, reader.GetString("LocalIP4Address"), reader.GetInt32("RingTimeShort"), reader.GetInt32("RingTimeLong"), reader.GetString("OutboundDigit"), reader.GetString("ProvisionExtension"), reader.GetString("ProvisionPassword"), reader.GetString("VoicemailExtension"), GetDDINumber(commWizardID, reader.GetString("VoicemailExtension")), reader.GetString("ReceptionExtension"), reader.GetString("ReceptionExtensions"), reader.GetString("SwitchboardExtension"), reader.GetString("ServiceExtensions"), reader.GetString("VMTreeExtension"), reader.GetString("EchoExtension"), reader.GetString("SpeakExtension"), reader.GetString("ParkCallExtension"), reader.GetString("ParkedCallExtensionRange"), reader.GetString("HandsetAdminPassword"), reader.GetString("ResyncPeriod"), reader.GetString("Shortcut1Text"), reader.GetString("Shortcut1Destination"), reader.GetString("AccountOverrideText"), reader.GetString("AccountOverrideDestination"), reader.GetString("Shortcut2Text"), reader.GetString("Shortcut2Destination"), reader.GetString("Shortcut3Text"), reader.GetString("Shortcut3TextShort"), reader.GetString("Shortcut3Destination"), reader.GetString("DialPatternFree"), reader.GetString("DialPatternLocal"), reader.GetString("DialPatternLocal2"), reader.GetString("DialPatternNational"), reader.GetString("DialPatternNational2"), reader.GetString("DialPatternInternationalShort"), reader.GetString("DialPatternPremium"), reader.GetString("DialPatternPremium2"), reader.GetString("DialPatternIncomingPrefix"));
            }

            //Disconnect from database if required
            Disconnect(disconnect);

            return callStream;
        }

        private static void CreateCustomer(Customer customer, bool disconnect = false)
        {
            //Create Customer
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("INSERT INTO Customer (Name, CentreNumber, AccountStatus, ExtRef1, ExtRef2, ActivationStatus, CreatedDate) SELECT '{0}', '{1}', '{2}', {3}, {4}, '{5}', {6} From Customer LIMIT 1;", customer.Name, customer.CallStream.CentreNumber, "Active", "MAX(ExtRef1)+1", "9999", "Active", "CURRENT_TIMESTAMP");                
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
            command.ExecuteNonQuery();

            //Get the CustomerID of the newly created Customer
            UpdateCustomerID(customer);

            //Disconnect from database if required
            Disconnect(disconnect);
        }

        private static void CreateAssociate(Associate associate, bool disconnect = false)
        {
            //Create Associate
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("INSERT INTO Associate (CustomerID, FirstName, LastName, Email, ExtRef1, ExtRef2, ActivationStatus, CreatedDate) VALUES({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7});", associate.Customer.CustomerID, associate.FirstName, associate.LastName, associate.Email, associate.Customer.ExtRef1, associate.Customer.ExtRef2, "Active", "CURRENT_TIMESTAMP");
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
            command.ExecuteNonQuery();

            //Get AssociateID for newly created Associate
            UpdateAssociateID(associate);

            //Disconnect from database if required
            Disconnect(disconnect);
        }

        private static void CreateExtension(FullExtension extension, bool disconnect = false)
        {
            //Create Extension
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("INSERT INTO Extension (Extension, ExtensionType, DDINumber, CommWizardID, ReferenceDDINumber) VALUES ('{0}', '{1}', '{2}', {3}, '{4}');", extension.Internal, "DDI", extension.DDI, extension.Associate.Customer.CallStream.CommWizardID, extension.ReferenceDDI);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
            command.ExecuteNonQuery();

            //Get ExtensionID for newly created Extension
            UpdateExtensionID(extension);

            //Disconnect from database if required
            Disconnect(disconnect);
        }

        private static void AssignExtension(FullExtension extension, bool disconnect = false)
        {
            //Get DialPlanID first
            UpdateDialPlanID(extension.Dialplan);

            //Create ExtensionDetail
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("INSERT INTO ExtensionDetail (ExtensionID, Status, CustomerID, TimeFrom, AssociateID, CreateSIP, Secret, PinFlag, DialPlanID, ComDialplanDelay, DiallingDelay, DateFormat, OutboudCIDNumber, TransferTo, EmailMsgTo, EmailNotificationTo, LineTypeID, ContactFirstName, ContactLastName, LanguageID, InternalNumber, MobileNumber, HomeNumber, EmergencyNumber, OtherNumber, Availability, CreatedDate, SIPSecret, OutgoingComExtension, AccountOverride) VALUES({0}, '{1}', {2}, '{3}', {4}, '{5}', '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', {26}, '{27}', {28}, '{29}');", extension.ExtensionID, "Active", extension.Associate.Customer.CustomerID, "2000-01-01 00:00:00", extension.Associate.AssociateID, 1, extension.Pin, extension.PinSetting, extension.Dialplan.DialplanID, extension.Dialplan.DialplanLegs[0].RingForSeconds, "5", extension.DateTimeFormat, extension.OutboundCallerID, extension.TransferTo, extension.Associate.Email, extension.Associate.Email, GetLineTypeID(extension.Type), extension.Associate.FirstName, extension.Associate.LastName, GetLanguageID(extension.Language), extension.Internal, extension.Mobile, extension.Home, extension.Emergency, extension.Other, "Available", "CURRENT_TIMESTAMP", extension.Secret, extension.ExtensionID, "999999");
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
            command.ExecuteNonQuery();

            //Disconnect from database if required
            Disconnect(disconnect);

            //Call webservice to propogate new Extension onto CallStream server
            UpdateExtension(extension);
        }

        private static void DeleteCustomer(Customer customer, bool disconnect = false)
        {
            //Get AssociateID if not defined
            if (UpdateCustomerID(customer))
            {
                //Delete Customer
                MySqlCommand command = new MySqlCommand("DELETE FROM Customer WHERE CustomerID = " + customer.CustomerID + ";", GetConnection());
                command.ExecuteNonQuery();

                //Disconnect from database if required
                Disconnect(disconnect);

                //Clear CustomerID
                ClearCustomerID(customer);
            }
        }

        private static void DeleteAssociate(Associate associate, bool disconnect = false)
        {
            //Get AssociateID if not defined
            if (UpdateAssociateID(associate))
            {
                //Delete Associate
                MySqlCommand command = new MySqlCommand("DELETE FROM Associate WHERE AssociateID = " + associate.AssociateID + ";", GetConnection());
                command.ExecuteNonQuery();

                //Disconnect from database if required
                Disconnect(disconnect);

                //Clear AssociateID
                ClearAssociateID(associate);
            }
        }

        private static void DeleteExtension(FullExtension extension, bool disconnect = false)
        {
            //Get the ExtensionID if not defined
            if (UpdateExtensionID(extension))
            {

                //Delete Extension
                MySqlCommand command = new MySqlCommand("DELETE FROM Extension WHERE ExtensionID = " + extension.ExtensionID + ";", GetConnection());
                command.ExecuteNonQuery();

                //Disconnect from database if required
                Disconnect(disconnect);

                //Clear ExtensionID
                ClearExtensionID(extension);
            }
        }

        private static void DeleteAssignment(FullExtension extension, bool disconnect = false)
        {
            //Get the ExtensionID if not defined
            if (UpdateExtensionID(extension))
            {
                //Delete ExtensionDetail
                MySqlCommand command = new MySqlCommand("UPDATE ExtensionDetail SET TimeTo = subdate(CURRENT_DATE, 1) WHERE ExtensionID = " + extension.ExtensionID + ";", GetConnection());
                command.ExecuteNonQuery();

                //Call webservice to delete Extension from CallStream server
                UpdateExtension(extension);

                command = new MySqlCommand("DELETE FROM ExtensionDetail WHERE ExtensionID = " + extension.ExtensionID + ";", GetConnection());
                command.ExecuteNonQuery();

                //Disconnect from database if required
                Disconnect(disconnect);
            }
        }

        private static bool UpdateCustomerID(Customer customer, bool disconnect = false)
        {
            if (customer.CustomerID > 0)
                return true;

            //Get CustomerID, ExtRef1 and ExtRef2
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT CustomerID, ExtRef1, ExtRef2 FROM Customer WHERE Name = '{0}' AND CentreNumber = '{1}' ORDER BY ExtRef1 DESC LIMIT 1;", customer.Name, customer.CallStream.CentreNumber);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                try
                {
                    customer.CustomerID = reader.GetInt32("CustomerID");
                    customer.ExtRef1 = reader.GetInt64("ExtRef1");
                    customer.ExtRef2 = reader.GetString("ExtRef2");
                }
                catch (MySqlException)
                { }
            }

            //Disconnect from database if required
            Disconnect(disconnect);

            if (customer.CustomerID > 0)
                return true;
            else
                return false;
        }

        private static bool UpdateAssociateID(Associate associate, bool disconnect = false)
        {
            if (associate.AssociateID > 0)
                return true;

            //Get AssociateID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT AssociateID FROM Associate WHERE FirstName = '{0}' AND LastName = '{1}' AND Email = '{2}' LIMIT 1;", associate.FirstName, associate.LastName, associate.Email);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                try
                {
                    reader.Read();
                    associate.AssociateID = reader.GetInt32("AssociateID");
                }
                catch (MySqlException)
                { }
            }

            //Disconnect from database if required
            Disconnect(disconnect);

            if (associate.AssociateID > 0)
                return true;
            else
                return false;
        }

        private static bool UpdateExtensionID(FullExtension extension, bool disconnect = false)
        {
            if (extension.ExtensionID > 0)
                return true;

            //Get ExtensionID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT ExtensionID FROM Extension WHERE Extension = '{0}' AND CommWizardID = '{1}' ORDER BY ExtensionID ASC LIMIT 1;", extension.Internal, extension.Associate.Customer.CallStream.CommWizardID);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                try
                {
                    reader.Read();
                    extension.ExtensionID = reader.GetInt32("ExtensionID");
                }
                catch (MySqlException)
                { }
            }

            //Disconnect from database if required
            Disconnect(disconnect);

            if (extension.ExtensionID > 0)
                return true;
            else
                return false;
        }

        private static bool UpdateExtension(FullExtension extension)
        {
            //Call CallStream webservice to progate Extension details
            if (webClient == null)
                webClient = new OzCommWizardServicesPortTypeClient("OzCommWizardServicesPort", EnvironmentApi.Settings.CallStream.WebServiceUrl);
            
            return webClient.com_update_extension(new CommWizardService.sessioninfo(), Convert.ToString(extension.ExtensionID));
        }

        private static bool UpdateDialPlanID(Dialplan dialplan, bool disconnect = false)
        {
            if (dialplan.DialplanID > 0)
                return true;

            //Get DialPlanID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT DialPlanID FROM DialPlan WHERE Name = '{0}' ORDER BY DialplanID ASC LIMIT 1", dialplan.Name);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                try
                {
                    reader.Read();
                    dialplan.DialplanID = reader.GetInt32("DialPlanID");
                }
                catch (MySqlException)
                { }
            }

            //Disconnect from database if required
            Disconnect(disconnect);

            if (dialplan.DialplanID > 0)
                return true;
            else
                return false;
        }

        private static void ClearCustomerID(Customer customer)
        {
            customer.CustomerID = 0;
            customer.ExtRef1 = 0;
            customer.ExtRef2 = null;
        }

        private static void ClearAssociateID(Associate associate)
        {
            associate.AssociateID = 0;
        }

        private static void ClearExtensionID(FullExtension extension)
        {
            extension.ExtensionID = 0;
            ClearDialplanID(extension.Dialplan);
        }

        private static void ClearExtensionHandset(FullExtension extension, bool disconnect = false)
        {
            //Get the ExtensionID if not defined
            UpdateExtensionID(extension);

            //Clear Handset details from Extension
            MySqlCommand command = new MySqlCommand("UPDATE Extension SET CurrentMacAddress = NULL, CurrentUserAgent = NULL WHERE ExtensionID = " + extension.ExtensionID + ";", GetConnection());
            command.ExecuteNonQuery();

            //Disconnect from database if required
            Disconnect(disconnect);

            //Call webservice to propogate Extension changes onto CallStream server
            UpdateExtension(extension);
        }

        private static void ClearDialplanID(Dialplan dialplan)
        {
            dialplan.DialplanID = 0;
        }

        private static string GetDDINumber(int commWizardID, string extension, bool disconnect = false)
        {
            return "01932792599";
            //Get DDINumber
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT DDINumber FROM Extension WHERE CommWizardID = {0} AND Extension = '{1}' LIMIT 1;", commWizardID, extension);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
            
            string ddiNumber;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                ddiNumber = reader.GetString("DDINumber");
            }
            
            //Disconnect from database if required
            Disconnect(disconnect);
            return ddiNumber;
        }

        private static int GetLineTypeID(string extensionType, bool disconnect = false)
        {
            //Get LineTypeID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT LineTypeID FROM LineType WHERE Description = '{0}' ORDER BY LineTypeID ASC LIMIT 1;", extensionType);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());
                
            int lineTypeID;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                lineTypeID = reader.GetInt32("LineTypeID");
            }

            //Disconnect from database if required
            Disconnect(disconnect);
            return lineTypeID;
        }

        private static int GetLanguageID(string language, bool disconnect = false)
        {
            //Get LanguageID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT LanguageID FROM Language WHERE Description = '{0}' ORDER BY LanguageID ASC LIMIT 1;", language);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            int languageID;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                languageID = reader.GetInt32("LanguageID");
            }

            //Disconnect from database if required
            Disconnect(disconnect);
            return languageID;
        }

        private static int GetCommWizardID(int locationID, bool disconnect = false)
        {
            //Get CommWizardID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT CommWizardID FROM CommWizards WHERE LocationID = {0} ORDER BY CommWizardID ASC LIMIT 1;", locationID);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            int commWizardID;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                commWizardID = reader.GetInt32("CommWizardID");
            }

            //Disconnect from database if required
            Disconnect(disconnect);
            return commWizardID;
        }

        private static int GetLocationID(string centreNumber, bool disconnect = false)
        {
            //Get LocationID
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("SELECT LocationID FROM Location WHERE ExternalLocationNumber = {0} ORDER BY LocationID ASC LIMIT 1;", centreNumber);
            MySqlCommand command = new MySqlCommand(builder.ToString(), GetConnection());

            int locationID;
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();
                locationID = reader.GetInt32("LocationID");
            }

            //Disconnect from database if required
            Disconnect(disconnect);
            return locationID;
        }

        private static MySqlConnection GetConnection()
        {
            if (connection != null && connection.State == ConnectionState.Open)
                return connection;

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Server={0}; Database={1}; Uid={2}; Pwd={3};", EnvironmentApi.Settings.DatabaseIPAddress, EnvironmentApi.Settings.DatabaseName, EnvironmentApi.Settings.DatabaseUsername, EnvironmentApi.Settings.DatabasePassword);
            connection = new MySqlConnection(builder.ToString());
            connection.Open();
            return connection;
        }

        private static void Disconnect(bool disconnect = true)
        {
            if (disconnect && connection != null && connection.State == ConnectionState.Open)
                connection.Close();
        }
    }
}