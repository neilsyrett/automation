﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class Dialplan
    {
        public int DialplanID { get; set; }

        public Collection<DialplanLeg> DialplanLegs { get; private set; }
        public string Name { get; private set; }

        public Dialplan(string name)
            : this(name, new Collection<DialplanLeg>())
        { }
        
        public Dialplan(string name, Collection<DialplanLeg> dialplanLegs)
        {
            Name = name;
            DialplanLegs = dialplanLegs;
        }
    }
}