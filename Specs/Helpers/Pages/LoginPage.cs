﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    class LoginPage : IUserInputPage
    {
        public IWebDriver Driver { get; private set; }

        public string Title { get { return Driver.Title; } }

        [FindsBy(How = How.Id, Using = "error")]
        private IList<IWebElement> errorText;
        public List<string> ErrorText
        {
            get
            {
                List<string> errorTextList = new List<string>();
                foreach (IWebElement element in errorText)
                {
                    errorTextList.Add(element.Text);
                }
                return errorTextList;
            }
        }

        [FindsBy(How = How.Id, Using = "user")]
        private IWebElement usernameField;

        [FindsBy(How = How.Id, Using = "pwd")]
        private IWebElement passwordField;

        [FindsBy(How = How.Id, Using = "btlogin")]
        private IWebElement loginButton;

        public void NavigateTo(IWebDriver webDriver)
        {
            Driver = webDriver;
            Driver.Navigate().GoToUrl(EnvironmentApi.Settings.CallStream.ManagerUrl + "/login");
            PageFactory.InitElements(Driver, this);

            if (!Title.Equals("CallStream Manager"))
            {
                throw new InvalidOperationException("This is not the Login Page. Current page is: " + Title);
            }
        }

        private IWebElement GetUserInputField(string fieldName)
        {
            fieldName = Regex.Replace(fieldName, @"\s+", "");
            switch (fieldName.ToLower())
            {
                case "username":
                    return usernameField;
                case "password" :
                    return passwordField;
            }
            return null;
        }
        private IWebElement GetButton(string buttonName)
        {
            buttonName = Regex.Replace(buttonName, @"\s+", "");
            switch (buttonName.ToLower())
            {
                case "login":
                    return loginButton;
            }
            return null;
        }

        public void EnterValueIntoInputField(string fieldName, string userInput)
        {
            IWebElement userInputField = GetUserInputField(fieldName);
            userInputField.Clear();
            userInputField.SendKeys(userInput);
        }

        public void ClearUserInputField(string fieldName)
        {
            GetUserInputField(fieldName).Clear();
        }

        public void ClickButton(string buttonName)
        {
            GetButton(buttonName).Click();
        }
    }
}
