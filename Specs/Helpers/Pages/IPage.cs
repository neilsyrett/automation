﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using System.Collections;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public interface IPage
    {
        IWebDriver Driver { get; }
        string Title { get; }
        List<string> ErrorText { get; }
        void NavigateTo(IWebDriver driver);
    }
}
