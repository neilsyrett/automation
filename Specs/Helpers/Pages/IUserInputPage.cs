﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    interface IUserInputPage : IPage
    {
        void EnterValueIntoInputField(string fieldName, string userInput);
        void ClearUserInputField(string fieldName);
        void ClickButton(string buttonName);
    }
}
