﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class StringExtensions
    {
        public static string ToTitleCase(this String text)
        {
            if (string.IsNullOrEmpty(text))
                return text;
            return text.Substring(0, 1).ToUpper() + text.Substring(1).ToLower();
        }

        public static string Random(this String text)
        {
            Random random = new Random();
            int maxLength = random.Next();
            return Random(text, maxLength);
        }

        public static string Random(this String text, int maxLength, int minLength = 0)
        {
            text = String.Empty;
            string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`¬¦!£$%^&*()_-=+[]{};'#:@~,./<>?|" + "\"" + @"\";
            Random random = new Random();
            int length = random.Next(minLength, maxLength);
            for (int i = 0; i <= length-1; i++)
            {
                int randomInt = random.Next(0, validChars.Length - 1);
                text += validChars.Substring(randomInt, 1);
            }
            return text;
        }

        public static int PositionToIndex(this string position)
        {
            int n;
            switch (position.ToLower())
            {
                case "first":
                case "1st":
                    n = 0;
                    break;
                case "second":
                case "2nd":
                    n = 1;
                    break;
                case "third":
                case "3rd":
                    n = 2;
                    break;
                case "fourth":
                case "4th":
                    n = 3;
                    break;
                case "fifth":
                case "5th":
                    n = 4;
                    break;
                case "sixth":
                case "6th":
                    n = 5;
                    break;
                case "seventh":
                case "7th":
                    n = 6;
                    break;
                case "eigth":
                case "8th":
                    n = 7;
                    break;
                case "ninth":
                case "9th":
                    n = 8;
                    break;
                case "tenth":
                case "10th":
                    n = 9;
                    break;
                default:
                    n = 0;
                    break;
            }

            return n;
        }

        public static Number NameToNumber(this String name)
        {
            switch (name.ToLower())
            {
                case "internal":
                    return Number.Internal;
                case "external":
                    return Number.External;
                case "mobile":
                    return Number.Mobile;
                case "home":
                    return Number.Home;
                case "emergency":
                    return Number.Emergency;
                case "other":
                    return Number.Other;
                case "voicemail ddi":
                    return Number.VoicemailDDI;
                case "voicemail":
                case "voicemail extension":
                    return Number.VoicemailExtension;
                case "reception ddi":
                    return Number.ReceptionDDI;
                case "reception":
                case "reception extension":
                    return Number.ReceptionExtension;
                default:
                    return Number.Number;
            }
        }

        public static string DdiToNumber(this String ddi)
        {
            string number = ddi.Substring(ddi.IndexOf('(') + 1);
            number = number.Replace(")", "");
            return number.Replace("-", "");
        }

        public static string DdiToReferenceNumber(this String ddi)
        {
            string number = ddi.Replace("+", "");
            number = number.Replace("(", "");
            number = number.Replace(")", "");
            number = number.Replace("-", "");
            number = number.Replace(" ", "");
            return number.Trim();
        }
    }
}
