﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class TranslationApi
    {
        public static List<string> ExtensionsToList(string extensions)
        {
            List<string> extList = new List<string>();
            int pos = extensions.IndexOf('@');
            int prev = 0;

            while (pos >= 0)
            {
                extList.Add(extensions.Substring(0, pos).Replace("SIP/", ""));
                prev = pos+1;
                pos = extensions.IndexOf('@', pos+1);
            }

            extList.Add(extensions.Substring(prev).Replace("SIP/", ""));

            return extList;
        }
    }
}
