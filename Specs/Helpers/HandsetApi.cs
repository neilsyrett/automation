﻿using Microsoft.XmlDiffPatch;
using Regus.SipTester;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Xml;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class HandsetApi
    {
        public static string GetInitialConfiguration(string make, string model)
        {
            string templateConfig = File.ReadAllText(GenerateInitalConfigurationFilePath(make, model));
            return SubstitueVariables(templateConfig);
        }

        public static string GetFullConfiguration(string make, string model, FullExtension extension)
        {
            string templateConfig = File.ReadAllText(GenerateFullConfigurationFilePath(make, model));
            return SubstitueVariables(templateConfig, extension);
        }

        public static string SendConfigurationRequest(string make, string model)
        {
            return SendConfigurationRequest(make, model, NetworkEngine.GetMacAddress(EnvironmentApi.Settings.NetworkAdaptorID).ToString());
        }

        public static string SendConfigurationRequest(string make, string model, string macAddress)
        {
            macAddress = macAddress.ToUpper();
            make = make.ToTitleCase();
            model = model.ToUpper();

            string url = GenerateConfigurationURL(make, model, macAddress);

            if (url.Substring(0, 4).ToLower() == "http")
            {
                using (WebClient client = new WebClient())
                {
                    switch (make)
                    {
                        case "Cisco":
                            client.Headers.Add("Host", EnvironmentApi.Settings.CallStream.AsteriskIPAddress + ":80");
                            switch (model)
                            {
                                case "SPA525G":
                                    client.Headers.Add("User-Agent", "Cisco/SPA525G-7.5.4 (AAA123456AA)");
                                    break;
                                case "SPA525G2":
                                    client.Headers.Add("User-Agent", "Cisco/SPA525G2-7.5.4 (BBB123456BB)");
                                    break;
                                case "SPA508G":
                                    client.Headers.Add("User-Agent", "Cisco/SPA508G-7.5.2b (" + macAddress + ")(CCC123456CC)");
                                    client.Headers.Add("x-CiscoIPPhoneModelName", "SPA508G");
                                    client.Headers.Add("x-CiscoIPPhoneDisplay", "128x64");
                                    client.Headers.Add("x-CiscoIPPhoneSDKVersion", "NA");
                                    break;
                                case "7960":
                                case "7970":

                                    break;
                            }
                            break;
                        case "Polycom":
                            client.Headers.Add("Host", EnvironmentApi.Settings.CallStream.AsteriskIPAddress);
                            client.Headers.Add("Accept", "*/*");
                            switch (model)
                            {
                                case "IP330":
                                    client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_330-UA/4.1.3.0052 Type/Application");
                                    break;
                                case "IP331":
                                    client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_331-UA/4.0.3.7562 Type/Application");
                                    break;
                                case "IP450":
                                    client.Headers.Add("User-Agent", "FileTransport PolycomSoundPointIP-SPIP_450-UA/4.1.3.0052 Type/Application");
                                    break;
                                case "IP5000":
                                    client.Headers.Add("User-Agent", "FileTransport PolycomSoundStationIP-SSIP_5000-UA/4.0.1.13681 Type/Application");
                                    break;
                                case "VVX500":
                                    client.Headers.Add("User-Agent", "PolycomSoundPointIP-VVX_500");
                                    break;
                                case "Duo":
                                    client.Headers.Add("User-Agent", "FileTransport PolycomSoundStation-SS_Duo-UA/4.0.0.27587 Type/Application");
                                    break;
                            }
                            break;
                        case "Snom":
                            client.Headers.Add("Host", EnvironmentApi.Settings.CallStream.AsteriskIPAddress);
                            client.Headers.Add("Accept", "*/*");
                            client.Headers.Add("Accept-Language", "en");
                            client.Headers.Add("Keep-Alive", "5");
                            switch (model)
                            {
                                case "370":
                                    client.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; snom370-SIP 8.4.35 1.1.3-u)");
                                    break;
                            }
                            break;
                    }
                    return client.DownloadString(url);
                }
            }
            else
            {
                string filepath = "";
                switch (make)
                {
                    case "Cisco":
                        switch (model)
                        {
                            case "7960":
                            case "7970":
                                filepath = "request.dat";
                                break;
                        }
                        break;
                }
                return TftpEngine.Transfer(url, filepath);
            }
        }

        public static bool CompareInitialConfigurationFiles(string make, string model, string config1, string config2)
        {
            make = make.ToTitleCase();
            model = model.ToString();
            string directory = EnvironmentApi.CreateDirectory("Differences");

            XmlDiff xmlDiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnorePrefixes | XmlDiffOptions.IgnoreWhitespace);
            using (XmlTextReader reader1 = new XmlTextReader(GenerateStreamFromString(config1)))
            using (XmlTextReader reader2 = new XmlTextReader(GenerateStreamFromString(config2)))
            using (XmlTextWriter diffWriter = new XmlTextWriter(directory + "Initial_" + make + "_" + model + ".xml", null))
            {
                bool result = xmlDiff.Compare(reader1, reader2, diffWriter);
                return result;
            }
        }

        public static bool CompareFullConfigurationFiles(string make, string model, string config1, string config2)
        {
            make = make.ToTitleCase();
            model = model.ToString();
            string directory = EnvironmentApi.CreateDirectory("Differences");

            XmlDiff xmlDiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnorePrefixes | XmlDiffOptions.IgnoreWhitespace);
            using (XmlTextReader reader1 = new XmlTextReader(GenerateStreamFromString(config1)))
            using (XmlTextReader reader2 = new XmlTextReader(GenerateStreamFromString(config2)))
            using (XmlTextWriter diffWriter = new XmlTextWriter(directory + "Full_" + make + "_" + model + ".xml", null))
            {
                bool result = xmlDiff.Compare(reader1, reader2, diffWriter);
                return result;
            }
        }

        private static string GenerateConfigurationURL(string make, string model, string macAddress)
        {
            make = make.ToTitleCase();
            model = model.ToUpper();
            macAddress = macAddress.ToUpper();
            string provisionUrl = EnvironmentApi.Settings.CallStream.ProvisionUrl;

            switch (make)
            {
                case "Cisco":
                    switch (model)
                    {
                        case "7960":
                        case "7970":
                            return "tftp://" + EnvironmentApi.Settings.CallStream.AsteriskIPAddress;
                        default:
                            return provisionUrl + make + "/" + model + "/" + macAddress + ".cfg";
                    }
                case "Polycom":
                    return provisionUrl + macAddress + ".cfg";
                case "Snom":
                    return provisionUrl + "-" + macAddress;
                default:
                    return null;
            }
        }

        private static string GenerateInitalConfigurationFilePath(string make, string model)
        {
            return @"ConfigurationFiles\" + make.ToTitleCase() + @"\Initial_" + model.ToUpper() + ".xml";
        }

        private static string GenerateFullConfigurationFilePath(string make, string model)
        {
            return @"ConfigurationFiles\" + make.ToTitleCase() + @"\Full_" + model.ToUpper() + ".xml";
        }

        private static string SubstitueVariables(string config, FullExtension extension = null)
        {
            config = SubstitueCallStreamVariables(config, EnvironmentApi.Settings.CallStream);
            config = SubstitueHandsetVariables(config, NetworkEngine.GetMacAddress(EnvironmentApi.Settings.NetworkAdaptorID));
            
            if (extension != null)
                config = SubstitueeExtensionVariables(config, extension);
            
            return config;
        }

        private static string SubstitueeExtensionVariables(string config, FullExtension extension)
        {
            config = config.Replace("##DISPLAYNAME##", extension.DisplayName);
            config = config.Replace("##EXTENSION##", extension.Internal);
            config = config.Replace("##USERNAME##", extension.Username);
            config = config.Replace("##SECRET##", extension.Secret);
            config = config.Replace("##PIN##", extension.Pin);
            config = config.Replace("##PINSETTING##", extension.PinSetting);
            config = config.Replace("##CISCODATEFORMAT##", extension.CiscoDateFormat);
            config = config.Replace("##CISCOTIMEFORMAT##", extension.CiscoTimeFormat);
            config = config.Replace("##SNOMDATEFORMAT##", extension.SnomDateFormat);
            config = config.Replace("##SNOMTIMEFORMAT##", extension.SnomTimeFormat);
            config = config.Replace("##POLYCOMDATEFORMAT##", extension.PolycomDateFormat);
            config = config.Replace("##POLYCOMTIMEFORMAT##", extension.PolycomTimeFormat);
            return config;
        }

        private static string SubstitueCallStreamVariables(string config, CallStream callStream)
        {
            config = config.Replace("##SERVERIP##", callStream.AsteriskIPAddress);
            config = config.Replace("##PROVISIONNUMBER##", callStream.ProvisionNumber);
            config = config.Replace("##HANDSETADMINPASSWORD##", callStream.HandsetAdminPassword);
            config = config.Replace("##PROVISIONUSERNAME##", callStream.ProvisionExtension.Username);
            config = config.Replace("##PROVISIONSECRET##", callStream.ProvisionExtension.Secret);
            config = config.Replace("##RESYNCPERIOD##", callStream.ResyncPeriod);
            config = config.Replace("##VOICEMAILNUMBER##", callStream.VoicemailExtension);
            config = config.Replace("##KEY3TEXT##", callStream.Key3Text);
            config = config.Replace("##KEY3DESTINATION##", callStream.Key3Destination);
            config = config.Replace("##KEY3ALTERNATIVETEXT##", callStream.Key3AlternativeText);
            config = config.Replace("##KEY3ALTERNATIVEDESTINATION##", callStream.Key3AlternativeDestination);
            config = config.Replace("##KEY4TEXT##", callStream.Key4Text);
            config = config.Replace("##KEY4DESTINATION##", callStream.Key4Destination);
            config = config.Replace("##KEY5TEXT##", callStream.Key5Text);
            config = config.Replace("##KEY5TEXTSHORT##", callStream.Key5TextShort);
            config = config.Replace("##KEY5DESTINATION##", callStream.Key5Destination);
            return config;
        }

        private static string SubstitueHandsetVariables(string config, PhysicalAddress macAddress)
        {
            config = config.Replace("##MACADDRESS##", macAddress.ToString().ToUpper());
            return config;
        }

        private static Stream GenerateStreamFromString(string text)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}