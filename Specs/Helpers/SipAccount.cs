﻿using System;
using Regus.SipTester;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public abstract class SipAccount
    {
        SipEngine sipEngine;
        public string Username { get; private set; }
        public string Secret { get; private set; }

        public Call LastCall
        {
            get
            {
                return sipEngine.LastCall; 
            }
        }

        public double VoicemailMessagesUnread
        {
            get
            {
                sipEngine.UpdateVoicemailMessageCounts();
                return sipEngine.VoicemailMessagesUnread;
            }
        }

        public double VoicemailMessagesRead
        {
            get
            {
                sipEngine.UpdateVoicemailMessageCounts();
                return sipEngine.VoicemailMessagesRead;
            }
        }

        public double VoicemailUrgentMessagesUnread
        {
            get
            {
                sipEngine.UpdateVoicemailMessageCounts();
                return sipEngine.VoicemailUrgentMessagesUnread;
            }
        }

        public double VoicemailUrgentMessagesRead
        {
            get
            {
                sipEngine.UpdateVoicemailMessageCounts();
                return sipEngine.VoicemailUrgentMessagesRead;
            }
        }

        public SipAccount(string username, string secret)
        {
            Username = username;
            Secret = secret;
            CreateSipEngine();
        }

        public bool Connect()
        {
            if (sipEngine != null)
                sipEngine.Connect(EnvironmentApi.Settings.CallStream.AsteriskIPAddress, Username, Secret, EnvironmentApi.Settings.NetworkAdaptorID, EnvironmentApi.Settings.SipPort, true);
            return sipEngine.IsConnected;
        }

        public void Dial(string number, bool outbondDigit = false)
        {
            
            if (outbondDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                sipEngine.Call(EnvironmentApi.Settings.CallStream.OutboundDigit + number, false);
            else
                sipEngine.Call(number, false);
        }

        public void Dial(FullExtension callExtension, string numberName, bool outboundDigit = false)
        {
            if (outboundDigit && !String.IsNullOrWhiteSpace(EnvironmentApi.Settings.CallStream.OutboundDigit))
                sipEngine.Call(EnvironmentApi.Settings.CallStream.OutboundDigit + callExtension.GetNumber(numberName), false);
            else
                sipEngine.Call(callExtension.GetNumber(numberName), false);
        }

        public Call AnswerNextCall()
        {
            return sipEngine.AnswerNextCall();
        }

        public void SendTone(char digit)
        {
            sipEngine.SendTone(digit);
        }

        public void SendTones(string digits)
        {
            sipEngine.SendTones(digits);
        }

        public string GetRemoteNumber()
        {
            return sipEngine.GetRemoteNumber(LastCall);
        }

        public string GetRemoteNumber(Call call)
        {
            return sipEngine.GetRemoteNumber(call);
        }

        public void Hangup()
        {
            sipEngine.EndCall();
        }

        public void HangupOutgoingCalls()
        {
            sipEngine.EndOutgoingCalls();
        }

        public void Disconnect()
        {
            if (sipEngine != null)
                sipEngine.Disconnect();
        }

        private void CreateSipEngine()
        {
            sipEngine = new SipEngine(false, false);
            sipEngine.SessionVersion = EnvironmentApi.Settings.SipVersion;
            sipEngine.SessionName = EnvironmentApi.Settings.SipSessionName;
            sipEngine.DialToneDelayMS = EnvironmentApi.Settings.DialToneDelayMS;
            sipEngine.AudioFilePath = EnvironmentApi.Settings.AudioFilePath;
            sipEngine.AudioOutputDevice = EnvironmentApi.Settings.AudioOutputDevice;

            #if DEBUG
                //se.MessageLog.CollectionChanged += ContentCollectionChanged;
            #endif
        }
    }
}
