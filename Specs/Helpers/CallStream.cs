﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class CallStream
    {
        public string CentreNumber { get; private set; }
        public int LocationID { get; private set; }
        public int CommWizardID { get; private set; }
        public string ManagerIPAddress { get; private set; }
        public string AsteriskIPAddress { get; private set; }
        public string AsteriskUsername { get; private set; }
        public string AsteriskPassword { get; private set; }
        public int RingTimeShort { get; private set; }
        public int RingTimeLong { get; private set; }
        public string OutboundDigit { get; private set; }
        public string ProvisionNumber { get; private set; }
        public string ProvisionPassword { get; private set; }
        public string MainNumber { get; private set; }
        private string mainDDI;
        public string MainDDI
        {
            get
            {
                return mainDDI;
            }
            set
            {
                mainDDI = value;
                MainNumber = value.DdiToNumber();
            }
        }
        public string NumberPrefix
        {
            get
            {
                return MainNumber.Substring(0, 7);
            }
        }
        public string VoicemailExtension { get; private set; }
        public string VoicemailDDI { get; private set; }
        public string ReceptionExtension { get; private set; }
        public List<string> ReceptionExtensions { get; private set; }
        public string SwitchboardExtension { get; private set; }
        public List<string> SwitchboardExtensions { get; private set; }
        public SipAccount ProvisionExtension { get; private set; }
        public string VMTree { get; private set; }
        public string Echo { get; private set; }
        public string Speak { get; private set; }
        public string ParkCall { get; private set; }
        public string ParkedCallsStart { get; private set; }
        public string ParkedCallsEnd { get; private set; }
        public int InboundCarrierDigits { get; private set; }
        public string HandsetAdminPassword { get; private set; }
        public string ResyncPeriod { get; private set; }
        public string Key3Text { get; private set; }
        public string Key3Destination { get; private set; }
        public string Key3AlternativeText { get; private set; }
        public string Key3AlternativeDestination { get; private set; }
        public string Key4Text { get; private set; }
        public string Key4Destination { get; private set; }
        public string Key5Text { get; private set; }
        public string Key5TextShort { get; private set; }
        public string Key5Destination { get; private set; }
        public string DialPatternFree { get; private set; }
        public string DialPatternLocal { get; private set; }
        public string DialPatternLocal2 { get; private set; }
        public string DialPatternNational { get; private set; }
        public string DialPatternNational2 { get; private set; }
        public string DialPatternInternationalShort { get; private set; }
        public string DialPatternPremium { get; private set; }
        public string DialPatternPremium2 { get; private set; }
        public string DialPatternIncomingPrefix { get; private set; }
        public string ProvisionUrl
        {
            get
            {
                return "http://" + AsteriskIPAddress + "/provisioning/";
            }
        }

        public string WebServiceUrl
        {
            get
            {
                return "http://" + AsteriskIPAddress + "/commwizard/CMLocalServices/services.php";
            }
        }

        public string ManagerUrl
        {
            get
            {
                return "http://" + EnvironmentApi.Settings.DatabaseIPAddress;
            }
        }

        public string LegacyManagerUrl
        {
            get
            {
                return "http://" + EnvironmentApi.Settings.DatabaseIPAddress + "/controlwizard/control/";
            }
        }

        private Collection<User> users = new Collection<User>();
        public Collection<User> Users
        {
            get
            {
                return users;
            }
            set
            {
                users = value;
            }
        }

        private Collection<Customer> customers = new Collection<Customer>();
        public Collection<Customer> Customers
        {
            get
            {
                return customers;
            }
            set
            {
                customers = value;
            }
        }

        public Collection<Associate> Associates
        {
            get
            {
                Collection<Associate> associates = new Collection<Associate>();
                foreach (Customer customer in Customers)
                {
                    foreach (Associate associate in customer.Associates)
                    {
                        associates.Add(associate);
                    }
                }
                return associates;
            }
        }

        public Collection<FullExtension> Extensions
        {
            get
            {
                Collection<FullExtension> extensions = new Collection<FullExtension>();
                foreach (Associate associate in Associates)
                {
                    foreach (FullExtension extension in associate.Extensions)
                    {
                        extensions.Add(extension);
                    }
                }
                return extensions;
            }
        }

        public CallStream(string centreNumber, string mainNumber, int inboundCarrierDigits, string asteriskUsername, string asteriskPassword, string provisionUsername, string provisionSecret, int locationID, int commWizardID, string ipAddress, int ringTimeShort, int ringTimeLong, string outboundDigit, string provisionNumber, string provisionPassword, string voicemailExtension, string voicemailDDI, string receptionExtension, string receptionExtensions, string switchboardExtension, string switchboardExtensions, string vmTree, string echo, string speak, string parkCall, string parkedCallExtensionRange, string handsetAdminPassword, string resyncPeriod, string key3Text, string key3Destination, string key3AlternativeText, string key3AlternativeDestination, string key4Text, string key4Destination, string key5Text, string key5TextShort, string key5Destination, string dialPatternFree, string dialPatternLocal, string dialPatternLocal2, string dialPatternNational, string dialPatternNational2, string dialPatternInternationalShort, string dialPatternPremium, string dialPatternPremium2, string dialPatternIncomingPrefix)
        {
            CentreNumber = centreNumber;
            LocationID = locationID;
            CommWizardID = commWizardID;
            AsteriskIPAddress = ipAddress;
            AsteriskUsername = asteriskUsername;
            AsteriskPassword = asteriskPassword;
            RingTimeShort = ringTimeShort;
            RingTimeLong = ringTimeLong;
            OutboundDigit = outboundDigit;
            ProvisionNumber = provisionNumber;
            ProvisionPassword = provisionPassword;
            MainDDI = mainNumber;
            VoicemailExtension = voicemailExtension;
            VoicemailDDI = voicemailDDI;
            ReceptionExtension = receptionExtension;
            ReceptionExtensions = TranslationApi.ExtensionsToList(receptionExtensions);
            SwitchboardExtension = switchboardExtension;
            SwitchboardExtensions = TranslationApi.ExtensionsToList(switchboardExtensions);
            ProvisionExtension = new ProvisionExtension(provisionUsername, provisionSecret);
            VMTree = vmTree;
            Echo = echo;
            Speak = speak;
            ParkCall = parkCall;
            ParkedCallsStart = parkedCallExtensionRange.Substring(0, parkedCallExtensionRange.IndexOf('-'));
            ParkedCallsEnd = parkedCallExtensionRange.Substring(parkedCallExtensionRange.IndexOf('-') + 1);
            InboundCarrierDigits = inboundCarrierDigits;
            HandsetAdminPassword = handsetAdminPassword;
            ResyncPeriod = resyncPeriod;
            Key3Text = key3Text;
            Key3Destination = key3Destination;
            Key3AlternativeText = key3AlternativeText;
            Key3AlternativeDestination = key3AlternativeDestination;
            Key4Text = key4Text;
            Key4Destination = key4Destination;
            Key5Text = key5Text;
            Key5TextShort = key5TextShort;
            Key5Destination = key5Destination;
            DialPatternFree = dialPatternFree;
            DialPatternLocal = dialPatternLocal;
            DialPatternLocal2 = dialPatternLocal2;
            DialPatternNational = dialPatternNational;
            DialPatternNational2 = dialPatternNational2;
            DialPatternInternationalShort = dialPatternInternationalShort;
            DialPatternPremium = dialPatternPremium;
            DialPatternPremium2 = dialPatternPremium2;
            DialPatternIncomingPrefix = dialPatternIncomingPrefix;
        }

        public User GetUser(string alias)
        {
            alias = alias.ToLower();
            foreach (User user in Users)
            {
                if (user.Alias.ToLower() == alias)
                    return user;
            }
            return null;
        }

        public FullExtension GetExtension(string extensionName)
        {
            extensionName = extensionName.ToTitleCase();
            foreach (FullExtension extension in Extensions)
            {
                if (extension.Name == extensionName)
                    return extension;
            }
            return null;
        }

        public string GetNumber(string number)
        {
            return GetNumber(number.NameToNumber());
        }

        public string GetNumber(Number number)
        {
            switch (number)
            {
                case Number.ReceptionExtension:
                    return ReceptionExtension;
                case Number.VoicemailExtension:
                    return VoicemailExtension;
                case Number.VoicemailDDI:
                    return VoicemailDDI;
                default:
                    return null;
            }
        }

        public void HangupOutgoing()
        {
            foreach (FullExtension extension in EnvironmentApi.Settings.CallStream.Extensions)
            {
                extension.HangupOutgoingCalls();
            }
        }
    }
}
