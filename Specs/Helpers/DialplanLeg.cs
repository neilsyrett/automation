﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class DialplanLeg
    {
        public Number Number { get; private set; }
        public int RingForSeconds { get; private set; }
        public int AnswerDelaySeconds { get; private set; }

        public DialplanLeg(Number number, int ringForSeconds)
            : this(number, ringForSeconds, 0) { }

        public DialplanLeg(Number number, int ringForSeconds, int answerDelaySeconds)
        {
            Number = number;
            RingForSeconds = ringForSeconds;
            AnswerDelaySeconds = answerDelaySeconds;
        }
    }
}
