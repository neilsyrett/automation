﻿using System;
using System.Collections.ObjectModel;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public long ExtRef1 { get; set; }
        public string ExtRef2 { get; set; }
        public string Name { get; private set; }
        public string AccountCode
        {
            get
            {
                return CustomerID + "_0";
            }
        }

        public CallStream CallStream { get; set; }

        private Collection<Associate> associates = new Collection<Associate>();
        public Collection<Associate> Associates
        {
            get
            {
                return associates;
            }
            set
            {
                associates = value;
            }
        }

        public Customer(string name, CallStream callStream)
        {
            Name = name;
            CallStream = callStream;
        }
    }
}
