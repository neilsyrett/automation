﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public enum Number { Internal, External, Mobile, Home, Emergency, Other, VoicemailExtension, VoicemailDDI, ReceptionExtension, ReceptionDDI, Number }
}
