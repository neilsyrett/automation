﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class Associate
    {
        public int AssociateID { get; set; }
        public Customer Customer { get; set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string AccountCode
        {
            get
            {
                return Customer.CustomerID + "_" + AssociateID;
            }
        }

        
        
        private Collection<FullExtension> extensions = new Collection<FullExtension>();
        public Collection<FullExtension> Extensions
        {
            get
            {
                return extensions;
            }
            set
            {
                extensions = value;
            }
        }

        public Associate(string firstName, string lastName, string email, Customer customer)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Customer = customer;
        }
    }
}
