﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    static class ConsoleApi
    {
        public static StreamWriter SW;
        public static StreamReader SR;
        public static Process Process;

        static ConsoleApi()
        {
            ProcessStartInfo psi = new ProcessStartInfo("cmd.exe")
            {
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                LoadUserProfile = true
            };

            Process = Process.Start(psi);

            SW = Process.StandardInput;
            SR = Process.StandardOutput;
        }
    }
}
