﻿using System;
using Regus.SipTester;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class FullExtension : SipAccount
    {
        public int ExtensionID { get; set; }
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            private set
            {
                name = value.ToTitleCase();
            }
        }
        public string Pin { get; private set; }
        public string PinSetting { get; private set; }
        public string OutboundPinSetting { get; private set; }
        public string Internal { get; private set; }
        public string External { get; private set; }
        public string ReferenceDDI { get; private set; }
        public string Mobile { get; private set; }
        public string Home { get; private set; }
        public string Emergency { get; private set; }
        public string Other { get; private set; }
        public string Type { get; private set; }
        public string Language { get; private set; }
        public string OutboundCallerID { get; private set; }
        public string DateTimeFormat { get; private set; }
        public string CiscoDateFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "d/m H|i":
                    case "d/m h|i":
                        return "day/month";
                    case "m/d H|i":
                    case "m/d h|i":
                        return "month/day";
                    default:
                        return null;
                }
            }
        }
        public string CiscoTimeFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "d/m H|i":
                    case "m/d H|i":
                        return "24hr";
                    case "d/m h|i":
                    case "m/d h|i":
                        return "12hr";
                    default:
                        return null;
                }
            }
        }
        public string SnomDateFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "m/d H|i":
                    case "m/d h|i":
                        return "on";
                    case "d/m H|i":
                    case "d/m h|i":
                        return "off";
                    default:
                        return null;
                }
            }
        }
        public string SnomTimeFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "d/m H|i":
                    case "m/d H|i":
                        return "on";
                    case "d/m h|i":
                    case "m/d h|i":
                        return "off";
                    default:
                        return null;
                }
            }
        }
        public string PolycomDateFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "d/m H|i":
                    case "d/m h|i":
                        return "DD/MM/YYYY";
                    case "m/d H|i":
                    case "m/d h|i":
                        return "MM/DD/YYYY";
                    default:
                        return null;
                }
            }
        }
        public string PolycomTimeFormat
        {
            get
            {
                switch (DateTimeFormat)
                {
                    case "d/m H|i":
                    case "m/d H|i":
                        return "1";
                    case "d/m h|i":
                    case "m/d h|i":
                        return "0";
                    default:
                        return null;
                }
            }
        }

        public Associate Associate { get; private set; }
        public Dialplan Dialplan { get; private set; }
        public string DisplayName
        {
            get
            {
                return Associate.FirstName + " " + Associate.LastName;
            }
        }

        private string ddi;
        public string DDI
        {
            get
            {
                return ddi;
            }
            set
            {
                External = value.DdiToNumber();
                ReferenceDDI = value.DdiToReferenceNumber();
                ddi = value;
            }
        }

        public string TransferTo
        {
            get
            {
                foreach (DialplanLeg leg in Dialplan.DialplanLegs)
                {
                    switch (leg.Number)
                    {
                        case Number.Mobile:
                            return Mobile;
                        case Number.Home:
                            return Home;
                        case Number.Emergency:
                            return Emergency;
                        case Number.Other:
                            return Other;
                    }
                }
                return Internal;
            }
        }

        public string OutboundCallerDisplayText
        {
            get
            {
                return Associate.FullName + " (" + Associate.Customer.Name +")";
            }
        }

        public string OutboundCallerDisplayNumber
        {
            get
            {
                return External.Substring(External.Length - Associate.Customer.CallStream.InboundCarrierDigits);
            }
        }

        public FullExtension(string name, string username, string secret, string pin, string pinSetting, string internalNumber, string ddi, string mobile, string home, string emergency, string other, string type, string language, string outboundCallerID, string dateTimeFormat, Associate associate, Dialplan dialplan) : base(username, secret)
        {
            Name = name;
            Pin = pin;
            PinSetting = pinSetting;
            Internal = internalNumber;
            DDI = ddi;
            Mobile = mobile;
            Home = home;
            Emergency = emergency;
            Other = other;
            Type = type;
            Language = language;
            OutboundCallerID = outboundCallerID;
            DateTimeFormat = dateTimeFormat;
            Associate = associate;
            Dialplan = dialplan;
        }

        public string GetNumber(string number)
        {
            return GetNumber(number.NameToNumber());
        }

        public string GetNumber(Number number)
        {
            switch (number)
            {
                case Number.Internal:
                    return Internal;
                case Number.External:
                    return External;
                case Number.Mobile:
                    return Mobile;
                case Number.Home:
                    return Home;
                case Number.Emergency:
                    return Emergency;
                case Number.Other:
                    return Other;
                case Number.ReceptionExtension:
                default:
                    return null;
            }
        }
    }
}
