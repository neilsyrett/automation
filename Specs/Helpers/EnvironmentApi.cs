﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Regus.CallStreamSmokeSuite.Environment;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public static class EnvironmentApi
    {
        public static IEnvironmentSettings Settings { get; set; }
    
        public static void InitialiseEnvironment()
        {
            if (System.IO.Directory.Exists(@"Results\"))
                System.IO.Directory.Delete(@"Results\", true);

            System.IO.Directory.CreateDirectory(@"Results\");
        }

        public static string CreateDirectory(string name)
        {
            string directory = @"Results\" + name + @"\";

            if (!System.IO.Directory.Exists(directory))
                System.IO.Directory.CreateDirectory(directory);

            return directory;
        }
    }
}
