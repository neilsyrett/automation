﻿using System;
using Regus.SipTester;

namespace Regus.CallStreamSmokeSuite.Helpers
{
    public class User
    {
        private string alias;
        public string Alias
        {
            get
            {
                return alias;
            }
            private set
            {
                alias = value.ToTitleCase();
            }
        }
        public string Username { get; private set; }
        public string Password { get; private set; }

        public User(string alias, string username, string password)
        {
            Alias = alias;
            Username = username;
            Password = password;
        }

        public string GetProperty(string propertyName)
        {
            switch (propertyName.ToLower())
            {
                case "username" :
                    return Username;
                case "password" :
                    return Password;
                default :
                    return null;
            }
        }
    }
}
