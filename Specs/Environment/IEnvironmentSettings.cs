﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Regus.CallStreamSmokeSuite.Helpers;

namespace Regus.CallStreamSmokeSuite.Environment
{
    public interface IEnvironmentSettings
    {
        CallStream CallStream { get; }

        int SipPort { get; }
        int SipVersion { get; }
        string SipSessionName { get; }
        int CallExpiryMS { get; }
        int DialToneDelayMS { get; }
        double CDRToleranceSeconds { get; }

        string AudioFilePath { get; }
        string AudioOutputDevice { get; }

        string NetworkAdaptorID { get; }
        
        string DatabaseIPAddress { get; }
        string DatabaseName { get; }
        string DatabaseUsername { get; }
        string DatabasePassword { get; }
        bool DatabaseFixturesCreate { get; }
        bool DatabaseFixturesDelete { get; }
    }
}