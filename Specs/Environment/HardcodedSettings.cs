﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Regus.CallStreamSmokeSuite.Helpers;

namespace Regus.CallStreamSmokeSuite.Environment
{
    public class HardcodedSettings : IEnvironmentSettings
    {
        public bool DatabaseFixturesCreate
        {
            //get { return true; }
            get { return false; }
        }

        public bool DatabaseFixturesDelete
        {
            //get { return true; }
            get { return false; }
        }

        public string AudioOutputDevice
        {
            get
            {
                return @"Speakers (Realtek High Definiti";
            }
        }

        public string NetworkAdaptorID
        {
            //Smoke Suite VirtualBox
            //get { return @"{20304E6D-7103-49F2-A86E-6F0B5A90D644}"; }

            //Neil's Laptop Ethernet
            //get { return @"{91E59756-BC5B-4C93-98F5-FA5462162E19}"; }

            //Neil's Laptop WiFi
            //get { return @"{6A48F440-54A3-41B7-8B1E-9B788FCCF8BE}"; }

            //Neil's Desktop
            get { return @"{BF371955-8929-4966-AE1B-178931D854C0}"; }

            //Apple USB
            //get { return @"{9C5AE98B-049B-480A-9C8F-A067EDD48BE1}"; }

            //Mark's USB Ethernet Adaptor
            //get { return @"{8F4BC58A-FA7D-4617-BE96-E844C63768B5}"; }

            //USB Ethernet Adaptor 1
            //get { return @"{7CE6FC43-720C-40C2-BFDE-970DB6272DAF}"; }

            //USB Ethernet Adaptor 2
            //get { return @"{634A185E-49F1-49DA-A4E1-5A1938EDAF8A}"; }

            //Blue USB Ethernet Adaptors
            //get { return @"{ACB7FFFA-B280-40CD-BF33-EE428BE66047}"; }
            //get { return @"{B91516F2-7595-4949-A24D-04FA55B0C22C}"; }

            //Neil's White USB Ethernet Adaptor - Ethernet 3
            //get { return @"{502896E0-A5A1-4174-85BC-6CEBC3754E16}"; }

        }

        private CallStream callStream;
        public CallStream CallStream
        {
            get
            {
                if (callStream == null)
                    Initialise();

                return callStream;
            }
            private set
            {
                callStream = value;
            }
        }

        public int SipPort
        {
            get { return 0; }
        }

        public int SipVersion
        {
            get { return 2; }
        }

        public string SipSessionName
        {
            get { return "Smoke Test"; }
        }

        public int CallExpiryMS
        {
            get { return 0; }
        }

        public int DialToneDelayMS
        {
            get { return 100; }
        }

        public double CDRToleranceSeconds
        {
            get { return 1; }
        }

        public string AudioFilePath
        {
            get { return @"C:\Program Files\Regus\Test Tools\SIP Tester\Audio\"; }
        }

        public string DatabaseIPAddress
        {
            get { return "10.79.127.10"; }
        }

        public string DatabaseName
        {
            get { return "CallStream"; }
        }

        public string DatabaseUsername
        {
            get { return "CallStream"; }
        }

        public string DatabasePassword
        {
            get { return "4Ng3l1na_J0l13"; }
        }

        private void Initialise()
        {
            Customer customer;
            Associate associate;
            FullExtension extension;
            Dialplan dialplan;
            Collection<DialplanLeg> dialplanLegs;

            CallStream = DatabaseApi.GetCallStream("6694", "+44(0)1932-79-2580", 10, "root", "CommWizABC_!", "Provisioning", "j0nvalJon24601");

            CallStream.Users.Add(new User("AM-CAM CSR", "callstream.amcam-csr", "Americas1"));
            CallStream.Users.Add(new User("AM-CAM Admin", "callstream.amcam-adm", "Americas1"));
            CallStream.Users.Add(new User("UK-EIRE CSR", "callstream.ukeire-cs", "England1"));
            CallStream.Users.Add(new User("UK-EIRE Admin", "callstream.ukeire-ad", "England1"));
            CallStream.Users.Add(new User("EU-AF CSR", "callstream.euaf-csr", "Sunshine1"));
            CallStream.Users.Add(new User("EU-AF Admin", "callstream.euaf-admi", "Sunshine1"));
            CallStream.Users.Add(new User("ASIA CSR", "callstream.asia-csr", "Helpdesk1"));
            CallStream.Users.Add(new User("ASIA Admin", "callstream.asia-admi", "Helpdesk1"));

            customer = new Customer("Smoke Suite 1", CallStream);
            CallStream.Customers.Add(customer);

            dialplanLegs = new Collection<DialplanLeg>();
            dialplanLegs.Add(new DialplanLeg(Number.Internal, 5));
            dialplanLegs.Add(new DialplanLeg(Number.Mobile, CallStream.RingTimeLong));
            dialplanLegs.Add(new DialplanLeg(Number.VoicemailExtension, 0, 1));
            dialplan = new Dialplan("Ring Internal, Ring External", dialplanLegs);
            associate = new Associate("Smoke Suite", "Office 1", "callstream.ci@regus.com", customer);
            customer.Associates.Add(associate);
            extension = new FullExtension("Office", "9001", "1234", "2580", "Never", "9001", "+44(0)1932-79-2596", "07763782590", "", "", "", "Office Company", "English", "Mine", "d/m H|i", associate, dialplan);
            associate.Extensions.Add(extension);

            dialplanLegs = new Collection<DialplanLeg>();
            dialplanLegs.Add(new DialplanLeg(Number.Internal, 5));
            dialplanLegs.Add(new DialplanLeg(Number.VoicemailExtension, 0, 1));
            dialplan = new Dialplan("Ring Internal, Voicemail", dialplanLegs);
            associate = new Associate("Smoke Suite", "Meeting Room 1", "callstream.ci@regus.com", customer);
            customer.Associates.Add(associate);
            extension = new FullExtension("Meeting Room", "9002", "1234", "2580", "Never", "9002", "+44(0)1932-79-2597", "07763782590", "", "", "", "Main Reception", "English", "Hidden", "d/m H|i", associate, dialplan);
            associate.Extensions.Add(extension);

            dialplanLegs = new Collection<DialplanLeg>();
            dialplanLegs.Add(new DialplanLeg(Number.Internal, 5));
            dialplanLegs.Add(new DialplanLeg(Number.ReceptionExtension, CallStream.RingTimeLong));
            dialplanLegs.Add(new DialplanLeg(Number.VoicemailExtension, 0, 1));
            dialplan = new Dialplan("Ring Internal, Ring Reception", dialplanLegs);
            associate = new Associate("Smoke Suite", "Secretary 1", "callstream.ci@regus.com", customer);
            customer.Associates.Add(associate);
            extension = new FullExtension("Secretary", "9003", "1234", "2580", "Never", "9003", "+44(0)1932-79-2598", "", "", "", "", "Office Individual", "English", "Mine", "d/m H|i", associate, dialplan);
            associate.Extensions.Add(extension);
        }
    }
}