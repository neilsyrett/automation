﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using Regus.CallStreamSmokeSuite.Helpers;
using System;

namespace Regus.CallStreamSmokeSuite.Environment
{
    public class ConfigurableSettings : IEnvironmentSettings
    {
        readonly dynamic settings;

        public ConfigurableSettings()
        {
            settings = XElement.Load(@"Environment\environment.settings.xml").ToDynamic();
        }

        private CallStream callStream;
        public CallStream CallStream
        {
            get
            {
                if (callStream == null)
                    Initialise();

                return callStream;
            }
            private set
            {
                callStream = value;
            }
        }

        private void Initialise()
        {
            CallStream = DatabaseApi.GetCallStream((string)settings.CallStream.CentreNumber, (string)settings.CallStream.MainNumber, (int)settings.CallStream.InboundCarrierDigits, (string)settings.CallStream.AsteriskUsername, (string)settings.CallStream.AsteriskPassword, (string)settings.CallStream.ProvisionUsername, (string)settings.CallStream.ProvisionSecret);

            IEnumerable<XElement> customersXML = settings.CallStream.Customers;
            foreach (XElement customerXML in customersXML)
            {
                dynamic cust = customerXML.ToDynamic();
                Customer newCustomer = new Customer((string)cust.Name, callStream);
                CallStream.Customers.Add(newCustomer);

                IEnumerable<XElement> associatesXML = cust.Associates;
                foreach (XElement associateXML in associatesXML)
                {
                    dynamic assoc = associateXML.ToDynamic();
                    Associate newAssociate = new Associate((string)assoc.FirstName, (string)assoc.LastName, (string)assoc.Email, newCustomer);
                    newCustomer.Associates.Add(newAssociate);

                    IEnumerable<XElement> extensionsXML = assoc.Extensions;
                    foreach (XElement extensionXML in extensionsXML)
                    {
                        dynamic ext = extensionXML.ToDynamic();
                        Dialplan newDialplan = new Dialplan((string)ext.Dialplan.Name);

                        IEnumerable<XElement> legsXML = ext.Dialplan.Legs;
                        foreach (XElement legXML in legsXML)
                        {
                            dynamic leg = legXML.ToDynamic();

                            if (leg.DelaySeconds != null && leg.PickupSeconds != null)
                            {
                                newDialplan.DialplanLegs.Add(new DialplanLeg(((string)leg.Number).NameToNumber(), (int)leg.DelaySeconds, (int)leg.PickupSeconds));
                            }
                            else if (leg.DelaySeconds != null)
                            {
                                newDialplan.DialplanLegs.Add(new DialplanLeg(((string)leg.Number).NameToNumber(), (int)leg.DelaySeconds));
                            }
                            else if (leg.PickupSeconds != null)
                            {
                                newDialplan.DialplanLegs.Add(new DialplanLeg(((string)leg.Number).NameToNumber(), 0, (int)leg.PickupSeconds));
                            }
                            else
                            {
                                if(newDialplan.DialplanLegs.Count < 1)
                                    newDialplan.DialplanLegs.Add(new DialplanLeg(((string)leg.Number).NameToNumber(), callStream.RingTimeShort));
                                else
                                    newDialplan.DialplanLegs.Add(new DialplanLeg(((string)leg.Number).NameToNumber(), callStream.RingTimeLong));
                            }
                        }
                        newAssociate.Extensions.Add(new FullExtension((string)ext.Name, (string)ext.Username, (string)ext.Secret, (string)ext.Pin, (string)ext.PinSetting, (string)ext.Extension, (string)ext.DDI, (string)ext.Mobile, (string)ext.Home, (string)ext.Emergency, (string)ext.Other, (string)ext.Type, (string)ext.Language, (string)ext.OutboundCallerID, "d/m H|i", newAssociate, newDialplan));
                    }
                }
            }
        }

        public int SipPort
        {
            get { return settings.Sip.Port; }
        }

        public int SipVersion
        {
            get { return settings.Sip.Version; }
        }

        public string SipSessionName
        {
            get { return settings.Sip.SessionName; }
        }

        public int CallExpiryMS
        {
            get { return settings.General.CallExpiryMilliseconds; }
        }

        public int DialToneDelayMS
        {
            get { return settings.General.DialToneDelayMilliseconds; }
        }

        public double CDRToleranceSeconds
        {
            get { return settings.General.CDRToleranceSeconds; }
        }

        public string AudioFilePath
        {
            get { return settings.Audio.FilePath; }
        }

        public string AudioOutputDevice
        {
            get { return settings.Audio.OutputDevice; }
        }

        public string NetworkAdaptorID
        {
            get { return settings.Network.AdaptorID; }
        }

        public string DatabaseIPAddress
        {
            get { return settings.Database.IPAddress; }
        }

        public string DatabaseName
        {
            get { return settings.Database.Name; }
        }

        public string DatabaseUsername
        {
            get { return settings.Database.Username; }
        }

        public string DatabasePassword
        {
            get { return settings.Database.Password; }
        }

        public bool DatabaseFixturesCreate
        {
            get
            {
                if (settings.Database.Fixtures.Create != null)
                    return Convert.ToBoolean((string)settings.Database.Fixtures.Create);
                else
                    return false;
            }
        }

        public bool DatabaseFixturesDelete
        {
            get
            {
                if (settings.Database.Fixtures.Delete != null)
                    return Convert.ToBoolean((string)settings.Database.Fixtures.Delete);
                else
                    return false;
            }
        }
    }
}